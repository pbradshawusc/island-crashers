##Firebase Database Structure

+ NOTE: Format of items is: key (valueType), if no value type is specified, then the key is a directory

+ users
    + uid
        + email (NSString *value)
        + password (NSString *value)
        + experience (int value)
        + currentGames (NSString *value) //The list of games will be stored as "num,num,etc" possible to switch to NSArray * if supported
+ games
    + currentGameNum (int value) //used when creating a new game
    + (game number)
        + currentTurn (int playerNumber) //1 or 2
        + map (NSString *mapName)
        + players
            + 1
                + defenseBonus (int value)
                + email (NSString *value) //the email used in the users subdirectory
             + 2
                + Same as #1
        + units
            + currentUnitNum (int num) //used when creating a new unit
            + unit1
                 + health (int value)
                + locationX (int value) //tile x-coordinate
                + locationY (int value) //tile y-coordinate
                + player (int value) //1 or 2, corresponds to playerNum in players subdirectory
                + type (NSString *value) //Used to identify what type of unit this is; other static data is held locally in unit class
            + unit2
                + Same as #1, continuing for the number of units on the game board