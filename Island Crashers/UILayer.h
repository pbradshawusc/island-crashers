//
//  UILayer.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/28/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCLayer.h"
#import "Unit.h"
#import "Building.h"
#import "CCUIViewWrapper.h"
#import "InfiniteScrollPicker.h"

@class GamePlayData;
@class MapLayer;

@interface UILayer : CCLayer {
    InfiniteScrollPicker *resourcesScrollPicker;
    InfiniteScrollPicker *troopsScrollPicker;
    InfiniteScrollPicker *structuresScrollPicker;
    CCUIViewWrapper *resourceManagerWrapper;
    
    CCMenuItem *itemEndTurn;
    Unit *displayedUnit;
    Building *displayedBuilding;
    
    GamePlayData *gpd;
    bool back;
    NSString *opName;
    
    //Money Label!
    CCLabelTTF *moneyLabel;
    MapLayer *map;
    
    //Unit Information Structures
    CCLabelTTF *unitLocation;
    CCLabelTTF *unitMovementRange;
    CCLabelTTF *unitAttackRange;
    CCLabelTTF *unitFirepower;
    CCLabelTTF *unitDefense;
    CCLabelTTF *unitHealth;
    CCLabelTTF *blinkingLabel;
    CCSprite *unitImage;
    CCMenu *unitStore;
    NSTimeInterval timeBlinkLabel;
    CCSprite *label;
}

-(void) DisplayUnit:(Unit *)u;
-(void) DisplayStructure:(Building*)b;
-(void) setResourceManagerWithTroops: (NSMutableArray *) troops; /*andStructures: (NSMutableArray *) structures andResources: (NSMutableArray *) resources;*/
-(void) clean;
-(void) setGamePlayData:(GamePlayData *)g;
-(void) showEndTurn;
-(void) refreshScene;
-(void) updateMoneyLabel;
-(void) setMapLayer:(MapLayer*)m;
-(void) showOpponent:(NSString *)op;
@end
