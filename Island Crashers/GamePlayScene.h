//
//  GamePlayScene.h
//  Island Crashers
//
//  Created by Edward Foux on 3/24/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCScene.h"
#import "GamePlayData.h"
#import "UILayer.h"
#import <GameKit/GameKit.h>
#import <Firebase/Firebase.h>

@interface GamePlayScene : CCScene{
    int gameNum;
    int playerNum;
    GamePlayData *gpd;
    UILayer *ui;
    
    bool loadedUsers,loadedTurn, loadedUnits, loadedStructures, loadedWinner, exited;
    
    Firebase *fTurn;
    FirebaseHandle *fTurnHandle;
    
    NSTimeInterval timeStarted;
}
@property int gameNumber;
@property int playerNumber;
-(id) initWithGameNum:(int)gn PlayerNum:(int)pn OpName:(NSString *)op;
-(GamePlayData *) getGamePlayData;
@end
