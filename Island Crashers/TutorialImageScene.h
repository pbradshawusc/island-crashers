//
//  TutorialImageScene.h
//  Island Crashers
//
//  Created by Alexa Rucks on 4/28/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCScene.h"
#import "cocos2d.h"

@interface TutorialImageScene : CCLayer
{
    int tutStage;
    CCSprite *background;
}
+(CCScene*)Scene;
@end
