//
//  RifleSoldier.h
//  Island Crashers
//
//  Created by Joseph Lin on 3/31/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "Soldier.h"

@interface RifleSoldier : Soldier

-(NSString *) getUnitType;
@end
