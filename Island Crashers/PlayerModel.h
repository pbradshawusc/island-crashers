//
//  PlayerModel.h
//  Island Crashers
//
//  Created by James Lynch on 4/13/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlayerModel : NSObject {
    int woodCount;
    int meatCount;
    int metalCount;
    Boolean isPlayersTurn;
    
    NSMutableArray* units;
    // TODO: NSArray for Unlocked/Available troops that player is able to buy
    
    NSMutableArray* structures;
}

@end
