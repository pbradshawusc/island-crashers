//
//  BazookaSoldier.m
//  Island Crashers
//
//  Created by Joseph Lin on 3/31/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "BazookaSoldier.h"

@implementation BazookaSoldier
- (id)init
{
    self = [super init];
    if (self) {
        soldier_action = @"walking";
        unit_type = @"bazooka";
        movement = 2;
        firepower = 4;
        defense = 3;
        AttackDistance = 1;
        attackANIM = @selector(setupAnim);
    }
    return self;
}
@synthesize attackAnimationSelector = attackANIM;
- (void) setupAnim
{
    NSLog(@"[SETUP ANIM]");
    NSString *color;
    if(OurUnit)
    {
        color = [NSString stringWithFormat:@"blue"];
    }
    else
    {
        color = [NSString stringWithFormat:@"red"];
    }
    [self removeChild:healthLabel cleanup:YES];
    NSString *health = [NSString stringWithFormat:@"%i", hp];
    healthLabel = [CCLabelTTF labelWithString:health fontName:@"Marker Felt" fontSize:10];
    [self addChild:healthLabel z:100];
    [healthLabel setColor:ccc3(255, 0, 0)];
    [healthLabel setPosition:ccp(-10,-10)];
    
    NSString * identifier;
    if (canMove){
        identifier=@"M";
    }else if (canAtt){
        identifier=@"A";
    }else{
        identifier=@"X";
    }
    
    [self removeChild:identifierLabel cleanup:YES];
    
    identifierLabel = [CCLabelTTF labelWithString:identifier fontName:@"Marker Felt" fontSize:10];
    [self addChild:identifierLabel z:100];
    [identifierLabel setColor:ccc3(255, 0, 0)];
    [identifierLabel setPosition:ccp(10,10)];
    
    
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: [NSString stringWithFormat:@"soldier-%@.plist", color]];
    [spriteSheet removeAllChildrenWithCleanup:YES];
    spriteSheet = [CCSpriteBatchNode batchNodeWithFile:[NSString stringWithFormat:@"soldier-%@.png", color]];
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    for (int i=1; i<=6; i++) {
        CCSpriteFrame* frame =[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                               [NSString stringWithFormat:@"soldier-bazooka-running-%@-frame-0%d.png",color,i]];
        [walkAnimFrames addObject: frame];
        //CGSize a = [CGSizeFromString(SCALE)];
        //(CCSpriteFrame*)[[walkAnimFrames objectAtIndex:i] setSize:a];//.scale = SCALE;
    }
    
    
    NSMutableArray *shootAnimFrames = [NSMutableArray array];
    for (int i=1; i<=6; i++) {
        [shootAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"soldier-bazooka-shooting-%@-frame-0%d.png",color,i]]];
    }
    CCAnimation *walkAnim = [CCAnimation
                             animationWithSpriteFrames:walkAnimFrames delay:0.1f];
    CCAnimation *attackAnim = [CCAnimation
                               animationWithSpriteFrames:shootAnimFrames delay:0.1f];
    walkAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:walkAnim]];
    attackAction = [CCRepeat actionWithAction:[CCAnimate actionWithAnimation:attackAnim] times:1];
    
    //[soldier_sprite removeAllChildren];
    //[soldier_sprite cleanup];
    
    //Switch between the various sprites
    if ([soldier_action isEqualToString:@"walking"])
    {
        soldier_sprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"soldier-bazooka-running-%@-frame-01.png", color]];
        soldier_sprite.scale = 2.0;
        //soldier_sprite.position = ccp(xlocation, ylocation);
        soldier_sprite.position = ccp(0, 0);
        [soldier_sprite runAction:self->walkAction];

    }
    else if ([soldier_action isEqualToString:@"shooting"])// if (isRunning)
    {
        NSLog(@"HELLO WORLD: 5");
        
        soldier_sprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"soldier-bazooka-shooting-%@-frame-01.png", color]];
        soldier_sprite.scale = 2.0;
        //soldier_sprite.position = ccp(xlocation, ylocation);
        soldier_sprite.position = ccp(0, 0);
        [soldier_sprite runAction:attackAction];
    }
    else {return;}
    
    [spriteSheet addChild:self->soldier_sprite];
    
    [self addChild:spriteSheet z:0];
    //[self draw];

}

-(NSString *) getUnitType
{
    return unit_type;
}

-(CCSprite *) getUnitSprite
{
    return soldier_sprite;
}
@end
