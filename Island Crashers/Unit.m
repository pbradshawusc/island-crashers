//
//  Unit.m
//  Island Crashers
//
//  Created by Alexa Rucks on 3/28/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "Unit.h"
#import "GameData.h"


@implementation Unit
-(id) init
{
    if(self = [super init])
    {
        hp = -1;
        xPos = -1;
        yPos = -1;
        OurUnit = true;
        NSString *health = [NSString stringWithFormat:@"%i", hp];
        
        healthLabel = [CCLabelTTF labelWithString:health fontName:@"Marker Felt" fontSize:10];
        [self addChild:healthLabel z:100];

        canMove = true;
        canAtt = false;
        //CGSize *textSize = [health sizeWithFont:font constrainedToSize:CGSizeMake(200, 200) lineBreakMode:UILineBreakModeWordWrap];
        //textSize.
       // hplb = [CCLayerColor layerWithColor:<#(ccColor4B)#> width:healthLabel. height:]
        //[self addChild:hplb z:100];
        //[healthLabel setPosition:ccp(self.position.x+healthLabel.contentSize.width/2, self.position.y+healthLabel.contentSize.height/2)];
    }
    return self;
}
@synthesize isOurUnit = OurUnit;
@synthesize health = hp;
@synthesize xloc = xPos;
@synthesize yloc = yPos;
@synthesize uType = unit_type;
@synthesize att = firepower;
@synthesize def = defense;
@synthesize walkAct = walkAction;
-(void) setCanAttackFalse
{
    canAtt = false;
}
-(void) setCanAttackTrue
{
    canAtt = true;
}
-(BOOL) getCanAttack
{
    return canAtt;
}
-(int) getAttackDistance
{
    return AttackDistance;
}
-(BOOL) isMovable
{

    return canMove;
}
-(void) hasMoved
{
    canMove = false;
}
-(void) setOurUnit: (BOOL) ismyteam
{
    OurUnit = ismyteam;
}
-(void) setID:(int) id
{
    ID = id;
}
-(void) newTurn
{
    canMove = true;
}
-(int) baseFirepower
{
    //Returns base firepower to scene to be increased by multiplier
    return firepower;
}
-(int) baseDefense
{
    //Returns base defense to scene to be increased by multiplier
    return defense;
}
-(NSString*) type
{
    return unit_type;
}

-(void) setPosition:(CGPoint) pos
{
    [super setPosition:pos];
    float tileWidth = [GameData GetGameData].tileWidth;
    [healthLabel setColor:ccc3(255, 255, 255)];
    [healthLabel setPosition:ccp(-10,-10)];
    //[hplb
}
-(void) setlocationX: (int)x Y: (int) y
{
    xPos = x;
    yPos = y;
}

-(void) scaleval:(float)_scale
{
    SCALE = _scale;
    self.scale = _scale;
}
-(void) draw
{
    /*
     CCLabelTTF *portray_hp = [CCLabelTTF labelWithString:(hp_string)fontName:@"American Typewriter" fontSize:10];
     portray_hp.color = ccYELLOW;
     self.position = ccp(size.width/10, size.height - 10);
     portray_hp.position = CGPointMake(self.position.x + self.boundingBox.size.width/2, self.position.y + self.boundingBox.size.height/2);
     [self addChild:portray_hp];
     */
    [super draw];
    float tileWidth = [GameData GetGameData].tileWidth;
    
    [healthLabel setPosition:ccp(-10, -10)];
    //[healthLabel setPosition:ccp((-tileWidth/2)+healthLabel.contentSize.width/2, (-tileWidth/2)+healthLabel.contentSize.height/2)];
    //[healthLabel draw];
}

-(CCSprite *) getSprite
{
    return soldier_sprite;
}

-(int) getXPosition
{
    return xPos;
}
-(int) getYPosition
{
    return yPos;
}

-(int) getMovement
{
    return movement;
}
-(void) setWalking
{
    soldier_action = @"walking";
}

@synthesize sprite = soldier_sprite;
@end
