//
//  TutorialImageScene.m
//  Island Crashers
//
//  Created by Alexa Rucks on 4/28/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "TutorialImageScene.h"
@class MainMenuScene;

@implementation TutorialImageScene
+(CCScene*)Scene
{
    CCScene *sc = [CCScene node];
    TutorialImageScene *tut = [[TutorialImageScene alloc] init];
    [sc addChild:tut];
    return sc;
}
-(id) init
{
    self = [super init];
    tutStage = 1;
    // ask director for the window size
    CGSize size = [[CCDirector sharedDirector] winSize];
    //Set the background for the layer
    background = [CCSprite spriteWithFile:@"Tutorial1"];
    background.scaleX = size.width/background.contentSize.width;
    background.scaleY = size.height/background.contentSize.height;
    background.position = ccp(size.width/2, size.height/2);
    [self addChild: background];
    self.touchEnabled = YES;
    return self;
}
- (BOOL) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    tutStage++;
    if(tutStage == 10)
    {
        //Done!
        [[CCDirector sharedDirector] replaceScene:[CCTransitionPageTurn transitionWithDuration:1.0 scene:[MainMenuScene node] backwards:YES]];
    }
    else
    {
        //Move to the next stage
        CGSize size = [[CCDirector sharedDirector] winSize];
        [self removeChild:background];
        background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"Tutorial%d",tutStage]];
        background.scaleX = size.width/background.contentSize.width;
        background.scaleY = size.height/background.contentSize.height;
        background.position = ccp(size.width/2, size.height/2);
        [self addChild: background];
    }
}
@end
