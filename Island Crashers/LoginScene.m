//
//  LoginScene.m
//  Island Crashers
//
//  Created by Alexa Rucks on 3/16/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "LoginScene.h"
#import "cocos2d.h"
#import "MainMenuScene.h"
#import "TutorialImageScene.h"

@implementation LoginScene
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	LoginScene *layer = [LoginScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
    [super init];
    
    self.isTouchEnabled = YES;
    data = [GameData GetGameData];
    [data logOut];
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    CCLabelTTF *titleLabel = [CCLabelTTF labelWithString:@"Please Log In" fontName:@"Marker Felt" fontSize:34];
    [titleLabel setPosition:ccp(size.width/2, size.height-30)];
    [self addChild:titleLabel];
    
    emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(60, 60, 200, 30)];
    [emailTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [emailTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [emailTextField setPlaceholder:@"Email"];
    emailWrapper = [CCUIViewWrapper wrapperForUIView:emailTextField];
    [self addChild:emailWrapper];
    [emailTextField setTextColor: [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]];
    [emailTextField setBackgroundColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1.0]];
    
    passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(60, 120, 200, 30)];
    [passwordTextField setPlaceholder:@"Password"];
    [passwordTextField setSecureTextEntry:YES];
    [passwordTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [passwordTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    passwordWrapper = [CCUIViewWrapper wrapperForUIView:passwordTextField];
    [self addChild:passwordWrapper];
    [passwordTextField setTextColor: [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]];
    [passwordTextField setBackgroundColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1.0]];
    
    submitButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [submitButton addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchDown];
    [submitButton setTitle:@"Log In" forState:UIControlStateNormal];
    submitButton.frame = CGRectMake(size.width/2 - 60, 200, 120.0, 40.0);
    [submitButton setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:100 alpha:0.5]];
    [submitButton setTitleColor:[UIColor colorWithWhite:255 alpha:1.0] forState:UIControlStateNormal];
    submitWrapper = [CCUIViewWrapper wrapperForUIView:submitButton];
    [self addChild:submitWrapper];
    
    timePassed = 0.0;
    
    return self;
}
-(void) submit
{
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    data.email = [[emailTextField text] copy];
    [data.email retain];
    //NSLog(data.email);
    data.password = [[passwordTextField text] copy];
    [data.password retain];
    //NSLog(data.password);
    
    connectingLabel = [CCLabelTTF labelWithString:@"Connecting..." fontName:@"Marker Felt" fontSize:34];
    [connectingLabel setPosition:ccp(size.width/2, size.height/5)];
    [self addChild:connectingLabel];
    
    [data logIn];
    
    [self scheduleUpdate];
}
-(void) create
{
    [data createAccountWithEmail:[emailTextField text] Password:[passwordTextField text]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[TutorialImageScene Scene]]];
    
    timePassed = 0.0;
    //[self scheduleUpdate];
}
-(void) update:(ccTime)delta
{
    timePassed += delta;
    if([data checkLoginStatus])
    {
        [self unscheduleUpdate];
        [self clean];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuScene node]]];
    }
    else if(timePassed > 5.0)
    {
        //NSLog(@"Timed out, please try again.");
        timePassed = 0.0;
        [self unscheduleUpdate];
        [self removeChild:connectingLabel];
        UIAlertView *noAccountAlert = [[UIAlertView alloc] initWithTitle:@"No Account Found" message:@"An account could not be found. Would you like to create one?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [noAccountAlert show];
        [noAccountAlert release];
    }
}
- (BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    return YES;
}
- (void) registerWithTouchDispatcher
{
    [[CCDirector sharedDirector].touchDispatcher addTargetedDelegate:self priority:INT_MIN+1 swallowsTouches:YES];
}
-(void) clean
{
    [self removeChild:emailWrapper cleanup:YES];
    emailWrapper = nil;
    [self removeChild:passwordWrapper cleanup:YES];
    passwordWrapper = nil;
    [self removeChild:submitWrapper cleanup:YES];
    submitWrapper = nil;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString * buttonOntitle=[alertView buttonTitleAtIndex:buttonIndex];
    if ([buttonOntitle isEqualToString:@"Yes"])
    {
        [submitButton removeTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchDown];
        [submitButton addTarget:self action:@selector(create) forControlEvents:UIControlEventTouchDown];
        [submitButton setTitle:@"Create Account" forState:UIControlStateNormal];
        [passwordTextField setText:@""];
    }
    else if ([buttonOntitle isEqualToString:@"No"])
    {
        [passwordTextField setText:@""];
    }
}
-(void) dealloc
{
    [self clean];
    [super dealloc];
}
@end
