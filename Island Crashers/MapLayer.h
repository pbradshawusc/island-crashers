//
//  MapLayer.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/28/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCLayer.h"
#import <GameKit/GameKit.h>
#import "BazookaSoldier.h"
#import "GamePlayData.h"

@interface MapLayer : CCLayer
{
    GamePlayData * gameData;
    float clickedXPos, clickedYPos;
    NSMutableDictionary* gametiles;
    NSMutableDictionary* buildingTiles2D;
    //These two booleans represent states
    bool isMovingAnObject;
    bool isAttacking;
    NSMutableArray* movementTiles;
    NSMutableArray* attackableTiles;
    NSMutableArray* capturableTiles;
    
    
    Unit* unitMoving;
    Unit* unitAttacking;
    
    //New selection helpers
    Unit* unitSelected;
    int xSelected, ySelected;

    NSTimeInterval timeTapBegins;
    NSTimeInterval timeLastMoved;
    
}

-(void) addUnit:(Unit*) u;
-(void) addStructure:(Building*) b;
-(id) initWithGamePlayData:(GamePlayData *)g;
-(void) initializeStructures;
@end
