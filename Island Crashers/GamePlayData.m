//
//  GamePlayData.m
//  Island Crashers
//
//  Created by Joseph Lin on 4/13/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "GamePlayData.h"
#import "GameData.h"
#import <Firebase/Firebase.h>

@implementation GamePlayData
@synthesize defenseBonus = myDefenseBonus;
@synthesize gameNumber = gameNum;
@synthesize playerNumber = myPlayerNum;
@synthesize currentTurn = currentTurnNum;
@synthesize currentMaterials = myMaterials;
@synthesize currentFood = myFood;
@synthesize currentMoney = myMoney;
@synthesize theWinner = winner;
- (id)initWithUILayer: (UILayer *) ui
{
    if (self = [self init])
    {
        uiLayer = ui;
    }
    return self;
}
- (id)init
{
    if (self = [super init]) {
        OurUnitArray = [[NSMutableArray alloc] init];
        EnemyUnitArray = [[NSMutableArray alloc] init];
        OurStructureArray = [[NSMutableArray alloc] init];
        EnemyStructureArray = [[NSMutableArray alloc] init];
    }
    return self;
}
-(void) addOurUnit: (Unit*) NewUnit
{
    [OurUnitArray addObject:NewUnit];
    [self updateTroopsScrollView];
}
-(void) addEnemyUnit: (Unit*) NewUnit
{
    [EnemyUnitArray addObject:NewUnit];
}
-(void) addOurStructure: (Building *) NewBuilding
{
    [OurStructureArray addObject:NewBuilding];
}
-(void) addEnemyStructure: (Building *) NewBuilding
{
    [EnemyStructureArray addObject:NewBuilding];
}
-(void) removeEnemyStructure: (Building *) OldBuilding
{
    [EnemyStructureArray removeObject:OldBuilding];
    [OurStructureArray addObject:OldBuilding];
}
-(void) removeUnit: (Unit*) DeadUnit
{
    if (DeadUnit.isOurUnit)
    {
        [OurUnitArray removeObject:DeadUnit];
    }
    else
    {
        [EnemyUnitArray removeObject:DeadUnit];
    }
    [self updateTroopsScrollView];
}

-(void) updateTroopsScrollView
{
    [uiLayer setResourceManagerWithTroops: OurUnitArray];
}

-(NSMutableArray *) getTroopsWithPlayerID: (int) playerNum
{
    if(playerNum == myPlayerNum)
    {
        return OurUnitArray;
    }
    else
    {
        return EnemyUnitArray;
    }
}

-(NSMutableArray *) getStructuresWithPlayerID: (int) playerNum
{
    if(playerNum == myPlayerNum)
    {
        return OurStructureArray;
    }
    else
    {
        return EnemyStructureArray;
    }
}

-(void) writeDataToFirebase
{
    //Save data at root game location
    NSString *filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/currentTurn/", gameNum];
    Firebase* f = [[Firebase alloc] initWithUrl:filepath];
    [f setValue:[NSNumber numberWithInt:currentTurnNum]];
    
    //Save winner at root game location
    filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/winner/", gameNum];
    f = [[Firebase alloc] initWithUrl:filepath];
    [f setValue:[NSNumber numberWithInt:winner]];
    
    //Save data at player location
    filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/players/%i/", gameNum, myPlayerNum];
    f = [[Firebase alloc] initWithUrl:filepath];
    [f setValue:@{@"food" : [NSNumber numberWithInt:myFood], @"materials" : [NSNumber numberWithInt:myMaterials], @"money" : [NSNumber numberWithInt:myMoney], @"defenseBonus" : [NSNumber numberWithInt:myDefenseBonus], @"email" : [NSString stringWithFormat:[GameData GetGameData].email]}];
    
    //Delete all current unit data
    filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/units/", gameNum];
    f = [[Firebase alloc] initWithUrl:filepath];
    [f removeValue];
    
    //Write in all unit data
    filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/", gameNum];
    f = [[Firebase alloc] initWithUrl:filepath];
    Firebase *f2 = [f childByAppendingPath:@"units"];
    for(Unit *u in OurUnitArray)
    {
        Firebase *f3 = [f2 childByAutoId];
        [f3 setValue:@{@"health" : [NSNumber numberWithInt:u.health], @"locationX" : [NSNumber numberWithInt:u.xloc], @"locationY" : [NSNumber numberWithInt:u.yloc], @"player" : [NSNumber numberWithInt:myPlayerNum], @"type" : [NSString stringWithFormat:u.uType]}];
    }
    for(Unit *u in EnemyUnitArray)
    {
        Firebase *f3 = [f2 childByAutoId];
        [f3 setValue:@{@"health" : [NSNumber numberWithInt:u.health], @"locationX" : [NSNumber numberWithInt:u.xloc], @"locationY" : [NSNumber numberWithInt:u.yloc], @"player" : [NSNumber numberWithInt:3-myPlayerNum], @"type" : [NSString stringWithFormat:u.uType]}];
    }
    
    //Delete all current structure data
    filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/structures/", gameNum];
    f = [[Firebase alloc] initWithUrl:filepath];
    [f removeValue];
    
    //Write in all structure data
    filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/", gameNum];
    f = [[Firebase alloc] initWithUrl:filepath];
    f2 = [f childByAppendingPath:@"structures"];
    for(Building *b in OurStructureArray)
    {
        Firebase *f3 = [f2 childByAutoId];
        [f3 setValue:@{@"health" : [NSNumber numberWithInt:b.hqintegrity], @"locationX" : [NSNumber numberWithInt:b.xloc], @"locationY" : [NSNumber numberWithInt:b.yloc], @"owner" : [NSNumber numberWithInt:myPlayerNum], @"type" : [NSString stringWithFormat:b.structureType], @"captureProgress" : [NSNumber numberWithBool:b.captureProgress]}];
    }
    for(Building *b in EnemyStructureArray)
    {
        Firebase *f3 = [f2 childByAutoId];
        [f3 setValue:@{@"health" : [NSNumber numberWithInt:b.hqintegrity], @"locationX" : [NSNumber numberWithInt:b.xloc], @"locationY" : [NSNumber numberWithInt:b.yloc], @"owner" : [NSNumber numberWithInt:3-myPlayerNum], @"type" : [NSString stringWithFormat:b.structureType], @"captureProgress" : [NSNumber numberWithBool:b.captureProgress]}];
    }
    
}

-(void) displayUnit:(Unit*)u
{
    [uiLayer DisplayUnit:u];
}

-(void) displayStructure:(Building*)b
{
    [uiLayer DisplayStructure:b];
}

-(int) getPlayerNum
{
    return myPlayerNum;
}
-(void) createFunds
{
    for(Building*b in OurStructureArray)
    {
        if([b.structureType isEqualToString:@"headquarters"])
        {
            myMoney += 150;
        }
        else
        {
            myMoney += 50;
        }
    }
}
@end
