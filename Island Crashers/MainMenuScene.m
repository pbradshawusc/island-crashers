//
//  MainMenuScene.m
//  Island Crashers
//
//  Created by Alexa Rucks on 3/15/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "MainMenuScene.h"
#import "AccountInfoScene.h"
#import "GameListScene.h"
#import "RifleSoldier.h"
#import "BazookaSoldier.h"
#import "HeadQuarters.h"
#import "TutorialImageScene.h"

@implementation MainMenuScene
-(id) init
{
    self = [super init];
    
    //Initialize the gameData
    data = [GameData GetGameData];
    
    // ask director for the window size
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    //Set the background for the layer
    CCSprite *background = [CCSprite spriteWithFile:@"MenuBackground.png"];
    background.position = ccp(size.width/2, size.height/2);
    background.scaleY = 0.95;
    [self addChild: background];
    
    
    CCLabelTTF *titleLabel = [CCLabelTTF labelWithString:@"Island Crashers" fontName:@"Marker Felt" fontSize:34];
    [titleLabel setPosition:ccp(size.width/2, size.height*7/9)];
    [titleLabel setColor: ccc3(0,0,0)];
    
    [self addChild:titleLabel];
    CCMenuItem *itemAccountInfo = [CCMenuItemFont itemWithString:@"Account Information" block:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionPageTurn transitionWithDuration:1.0 scene:[AccountInfoScene node] ]];
    }];
    CCMenuItem *startNewGame = [CCMenuItemFont itemWithString:@"Game Menu" block:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipY transitionWithDuration:0.5 scene:[GameListScene node]]];
    }];
    CCMenuItem *itemTutorial = [CCMenuItemFont itemWithString:@"Replay Tutorial" block:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipY transitionWithDuration:0.5 scene:[TutorialImageScene Scene]]];
    }];
           CCMenu *menu = [CCMenu menuWithItems:itemAccountInfo, startNewGame, itemTutorial, nil];
    [menu alignItemsVerticallyWithPadding:20];
    [menu setPosition:ccp( size.width/2, size.height/2 - 50)];
    
    // Add the menu to the layer
    [self addChild:menu];
    [menu setColor: ccc3(0,0,0)];
    
    return self;
}
@end
