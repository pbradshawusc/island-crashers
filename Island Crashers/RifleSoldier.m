//
//  RifleSoldier.m
//  Island Crashers
//
//  Created by Joseph Lin on 3/31/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "RifleSoldier.h"

@implementation RifleSoldier
- (id)init
{
    self = [super init];
    if (self) {
        soldier_action = @"walking";
        unit_type = @"rifle";
        movement = 3;
        firepower = 2;
        defense = 3;
        AttackDistance = 1;
    }
    return self;
}
-(void) setupAnim
{

    CGSize size = [[CCDirector sharedDirector] winSize];
    
    NSString *color;
    if(OurUnit)
    {
        color = [NSString stringWithFormat:@"blue"];
    }
    else
    {
        color = [NSString stringWithFormat:@"red"];
    }
    
    [self removeChild:healthLabel cleanup:YES];
    NSString *health = [NSString stringWithFormat:@"%i", hp];
    healthLabel = [CCLabelTTF labelWithString:health fontName:@"Marker Felt" fontSize:10];
    [self addChild:healthLabel z:100];
    [healthLabel setColor:ccc3(255, 0, 0)];
    [healthLabel setPosition:ccp(-10,-10)];
    
    NSString * identifier;
    if (canMove){
        identifier=@"M";
    }else if (canAtt){
        identifier=@"A";
    }else{
        identifier=@"X";
    }
    
    [self removeChild:identifierLabel cleanup:YES];
    
    identifierLabel = [CCLabelTTF labelWithString:identifier fontName:@"Marker Felt" fontSize:10];
    [self addChild:identifierLabel z:100];
    [identifierLabel setColor:ccc3(255, 0, 0)];
    [identifierLabel setPosition:ccp(10,10)];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: [NSString stringWithFormat:@"soldier-%@.plist", color]];
    [spriteSheet removeAllChildrenWithCleanup:YES];
    spriteSheet = [CCSpriteBatchNode batchNodeWithFile:[NSString stringWithFormat:@"soldier-%@.png", color]];
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    for (int i=1; i<=6; i++) {
        [walkAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"soldier-%@-frame-0%d.png",color,i]]];
    }
    
    
    NSMutableArray *shootAnimFrames = [NSMutableArray array];
    for (int i=1; i<=5; i++) {
        [shootAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"soldier-shooting-%@-frame-0%d.png",color,i]]];
    }
    CCAnimation *walkAnim = [CCAnimation
                             animationWithSpriteFrames:walkAnimFrames delay:0.1f];
    CCAnimation *attackAnim = [CCAnimation
                               animationWithSpriteFrames:shootAnimFrames delay:0.1f];
    walkAction = [CCRepeatForever actionWithAction:
                  [CCAnimate actionWithAnimation:walkAnim]];
    attackAction = [CCRepeatForever actionWithAction:
                    [CCAnimate actionWithAnimation: attackAnim]];
    
    //[soldier_sprite removeAllChildren];
    //[soldier_sprite cleanup];
    
    //Switch between the various sprites
    if ([soldier_action isEqualToString:@"walking"])
    {
        soldier_sprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"soldier-%@-frame-01.png",color]];
        soldier_sprite.scale = 2.0;
        soldier_sprite.position = ccp(0, 0);
        [soldier_sprite runAction:self->walkAction];
    }
    else if ([soldier_action isEqualToString:@"shooting"])// if (isRunning)
    {
        soldier_sprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"soldier-shooting-%@-frame-01.png",color]];
        soldier_sprite.position = ccp(0,0);
        [soldier_sprite runAction:attackAction];
    }
    else {return;}
    
    [spriteSheet addChild:self->soldier_sprite];
    
    [self addChild:spriteSheet z:0];

}
-(NSString *) getUnitType
{
    return unit_type;
}
@end
