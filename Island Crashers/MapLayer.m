//
//  MapLayer.m
//  Island Crashers
//
//  Created by Alexa Rucks on 3/28/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "MapLayer.h"
#import "AppDelegate.h"
#import "GameData.h"
#import "RifleSoldier.h"
#import "MovableSprite.h"
#import "Building.h"
#import "HeadQuarters.h"
#import "Barracks.h"
#import "ResultsScene.h"
@interface MapLayer()

@property (strong) CCTMXTiledMap *tileMap;
@property (strong) NSMutableDictionary *mountainList;

@property (strong) CCTMXLayer *background;

// Inside the HelloWorldLayer interface with the other private properties
@property (strong) CCTMXLayer *meta;
@property (strong) CCTMXLayer *buildings;




@end
CCTMXMapInfo * mapInfo;
float distanceInitial,distance;
float lastGoodDistance;

//NSString* hqString = [NSString stringWithFormat:@"headquarters-%d", ];
//NSString* barrackString = @"barracks-%d";
NSTimeInterval      mLastTapTime;

@implementation MapLayer


-(id) initWithGamePlayData:(GamePlayData *)g
{
    if(self = [self init])
    {
        gameData = g;
        //JUST FOR TESTING
        /*
        Building *myHQ = [[HeadQuarters alloc] init];
        [myHQ setOurBuilding:true];
        myHQ.xloc = 0;
        myHQ.yloc = 0;
        myHQ.hqintegrity = 10;
        [self addStructure:myHQ];
        [gameData addOurStructure:myHQ];
        
        Building *enemyHQ = [[HeadQuarters alloc] init];
        [enemyHQ setOurBuilding:true];
        enemyHQ.xloc = 19;
        enemyHQ.yloc = 19;
        enemyHQ.hqintegrity = 10;
        [self addStructure:enemyHQ];
        [gameData addEnemyStructure:enemyHQ];
         */
        //END JUST FOR TESTING
    }
    return self;
}
-(int)convertCGPointToTileCoordinateX: (CGPoint)clickPoint
{
    int x=(int)(20*clickPoint.x/(self.tileMap.contentSize.width*self.tileMap.scaleX));
    return x;
}
-(int)convertCGPointToTileCoordinateY: (CGPoint)clickPoint
{
    int y=(int)(20*clickPoint.y/(self.tileMap.contentSize.height*self.tileMap.scaleY));
    return y;
}
//0 to 19
-(CGPoint)convertTileToCGPoint: (double)i Y:(double)j
{
    if (i < 0 || j < 0 || i > 19 || j > 19)
    {
        NSLog(@"INVALID LOCATION INPUTTED");
        return ccp(0,0);
    }
    float xpt = i * self.tileMap.contentSize.width*self.tileMap.scaleX/20;
    float ypt = j * self.tileMap.contentSize.height*self.tileMap.scaleY/20;
    CGPoint location = ccp(xpt,ypt);
    return location;
}
-(id) init
{
    if (self = [super init])
    {
        CGSize size = [CCDirector sharedDirector].winSize;
        //Set the background for the layer
        CCSprite *b = [CCSprite spriteWithFile:@"EndlessWater.png"];
        b.position = ccp(size.width/2, size.height/2);
        [self addChild: b z:-100];
        
        isMovingAnObject = false;
        isAttacking = false;
        unitMoving = nil;
        
        
        self.touchEnabled = YES;
        self.tileMap = [CCTMXTiledMap tiledMapWithTMXFile:@"Map.tmx"];
        
        //get CCTMXMapInfo object -- TMXTiledMap DOES NOT Contain the tile image properties
        mapInfo = [CCTMXMapInfo formatWithTMXFile: @"Map.tmx"];
        
        
        self.tileMap.scale = 5.0f;
        self.position = ccp((size.width/2)-(self.tileMap.scale*self.tileMap.contentSize.width/2), size.height/2-(self.tileMap.scale*self.tileMap.contentSize.height/2));
        self.background = [self.tileMap layerNamed:@"ground"];
        gametiles = [[NSMutableDictionary alloc]init];
        buildingTiles2D = [[NSMutableDictionary alloc] init];
        
        
        // In init, right after loading background
        self.meta = [self.tileMap layerNamed:@"Collidable"];
        _meta.visible = NO;
        self.buildings=[self.tileMap layerNamed:@"buildings"];
        _buildings.visible=NO;
        
        
        
        
        [self parseTMX];
        [self addChild:_tileMap z:-1];
        [self.tileMap setAnchorPoint:ccp(0,0)];
        
        mLastTapTime = [NSDate timeIntervalSinceReferenceDate];
        
        
        
        /*
        HeadQuarters *myHQ = [[HeadQuarters alloc] init];
        [myHQ setOurBuilding:true];
        CGPoint HQLocation = [self convertTileToCGPoint:0 Y:0];
        NSString *locationkey = [NSString stringWithFormat:@"0, 0"];
        [gametiles setObject:[NSString stringWithFormat:@"HQ-%i",[gameData getPlayerNum]] forKey: locationkey];
        [myHQ setupAnim];
        [myHQ setPosition:HQLocation];
        [self addChild:myHQ z:50];
        HeadQuarters* enemyHQ = [[HeadQuarters alloc] init];
        [enemyHQ setOurBuilding:false];
        CGPoint HQLocation2 = [self convertTileToCGPoint:19 Y:19];
        [enemyHQ setupAnim];
        [enemyHQ setPosition:HQLocation2];
        [self addChild: enemyHQ z:50];
        NSString *locationkey2 = [NSString stringWithFormat:@"19, 19"];
        [gametiles setObject:[NSString stringWithFormat:@"HQ-%i",3-[gameData getPlayerNum]] forKey: locationkey2];
         */
    }
    return self;
}
- (void) parseTMX
{
    //20x20
    for (int i = 0 ; i <= 19; i++)
        for (int j = 0; j <= 19; j++)
        {
            float xpt = i * self.tileMap.contentSize.width*self.tileMap.scaleX/20;
            float ypt = j * self.tileMap.contentSize.height*self.tileMap.scaleY/20;
            CGPoint location = ccp(xpt,ypt);
            int x=(int)(20*location.x/(self.tileMap.contentSize.width*self.tileMap.scaleX));
            int y=(int)(20*location.y/(self.tileMap.contentSize.height*self.tileMap.scaleY));
            
            NSString *locationkey = [NSString stringWithFormat:@"%d, %d", x, y];
            
            [gametiles setObject: @"empty" forKey: locationkey];
        }
}
-(Unit*) unitForLocationX:(int)x Y:(int)y
{
    NSMutableArray * troops = [gameData getTroopsWithPlayerID: [gameData getPlayerNum]];
    Unit * soldier = nil;
    for (int i = 0; i < [troops count]; i++)
    {
        if ([[troops objectAtIndex: i] getXPosition] == x
            && [[troops objectAtIndex: i] getYPosition] == y)
        {
            soldier = [troops objectAtIndex: i];
        }
    }
    
    if(soldier != nil)
    {
        return soldier;
    }
    
    troops = [gameData getTroopsWithPlayerID: (3-[gameData getPlayerNum])];
    for (int i = 0; i < [troops count]; i++)
    {
        if ([[troops objectAtIndex: i] getXPosition] == x
            && [[troops objectAtIndex: i] getYPosition] == y)
        {
            soldier = [troops objectAtIndex: i];
        }
    }
    
    return soldier;
}
-(Building *) structureForLocationX:(int)x Y:(int)y
{
    NSMutableArray* buildings = [gameData getStructuresWithPlayerID: [gameData getPlayerNum]];
    for(Building* b in buildings)
    {
        if(b.xloc == x && b.yloc == y)
        {
            return b;
        }
    }
    buildings = [gameData getStructuresWithPlayerID: 3-[gameData getPlayerNum]];
    for(Building* b in buildings)
    {
        if(b.xloc == x && b.yloc == y)
        {
            return b;
        }
    }
    return nil;
}
/*Here's what is happening:
 1 click: moves map
 2 clicks: 
 checks if we're in moving state. If so, checks if click was in moving state area. If so, set valid move to true.
 If valid move is true, and we've already clicked on an object
 
 */
- (BOOL) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([touches count]==2)
    {
        NSArray *touchArray = [touches allObjects];
        UITouch *fingerOne = [touchArray objectAtIndex:0];
        UITouch *fingerTwo = [touchArray objectAtIndex:1];

        [self setAnchorPoint:ccp(0,0)];
        distanceInitial=[self calculate:fingerOne calculate2:fingerTwo];
        NSLog(@"distance initial %f",distanceInitial);
        return YES;
    }
    else if([touches count] == 1)
    {
        //NEW SYSTEM
        //Mark the time the touch started
        timeTapBegins = [NSDate timeIntervalSinceReferenceDate];
        timeLastMoved = [NSDate timeIntervalSinceReferenceDate];
        for(UITouch *touch in touches)
        {
            CGPoint location = [self convertTouchToNodeSpace: touch];
            clickedXPos = location.x;
            clickedYPos = location.y;
            return NO;
        }
        return NO;
    }
    return YES;
}

-(BOOL)isValidMove:(int)xCoord :(int)yCoord{
    CGPoint tileCoord = ccp(xCoord,abs(yCoord-19));
    int tileGid = [_meta tileGIDAt:tileCoord];
    if (tileGid)
    {
        NSDictionary *properties = [_tileMap propertiesForGID:tileGid];
        if (properties)
        {
            NSString *collision = [properties valueForKey:@"Access"];
            //properties[@"Accesss"];
            if (collision && [collision isEqualToString:@"False"])
            {
                NSLog(@"Made it!");
                return NO;
            }
        }
    }
    return YES;
}

-(void) addStructure:(Building*) b
{
    float xLocation = b.xloc * self.tileMap.tileSize.width * self.tileMap.scaleX/2;
    float yLocation = b.yloc * self.tileMap.tileSize.height* self.tileMap.scaleY/2;
    [self addChild:b z:1];
    [b setPosition:ccp(xLocation, yLocation)];
    [b setupAnim];
    NSString *locationkey = [NSString stringWithFormat:@"%d, %d", b.xloc, b.yloc];
    if([b isOurBuilding])
    {
        NSString* buildingType = b.structureType;
        if ([buildingType isEqualToString:@"barracks"])
            [buildingTiles2D setObject:[NSString stringWithFormat:@"barracks-%i",[gameData getPlayerNum]] forKey: locationkey];
        else if ([buildingType isEqualToString:@"headquarters"])
            [buildingTiles2D setObject:[NSString stringWithFormat:@"headquarters-%i",[gameData getPlayerNum]] forKey: locationkey];
    }
    else
    {
        NSString * buildingType = b.structureType;
        if ([buildingType isEqualToString:@"barracks"])
            [buildingTiles2D setObject:[NSString stringWithFormat:@"barracks-%i",3-[gameData getPlayerNum]] forKey: locationkey];
        else if ([buildingType isEqualToString:@"headquarters"])
            [buildingTiles2D setObject:[NSString stringWithFormat:@"headquarters-%i",3-[gameData getPlayerNum]] forKey: locationkey];
    }
}

-(void) addUnit:(Unit*) u
{
    float xLocation = u.xloc * self.tileMap.tileSize.width * self.tileMap.scaleX/2;
    float yLocation = u.yloc * self.tileMap.tileSize.height* self.tileMap.scaleY/2;
    [self addChild:u z:100];
    [u setPosition:ccp(xLocation + 20, yLocation +  20)];
    if([u.uType isEqualToString:@"bazooka"])
    {
        BazookaSoldier *b = ((BazookaSoldier *) u);
        [b setupAnim];
    }
    else if([u.uType isEqualToString:@"rifle"])
    {
        RifleSoldier *r = ((RifleSoldier *)u);
        [r setupAnim];
    }
    else
    {
        [u setupAnim];
    }
    NSString *newkey = [NSString stringWithFormat:@"%d, %d", u.xloc, u.yloc];
    int playerNum = 3;
    if(u.isOurUnit)
    {
        playerNum = [gameData getPlayerNum];
    }
    else
    {
        playerNum -= [gameData getPlayerNum];
    }
    NSString *obj = [NSString stringWithFormat:@"%@-%d", u.uType, playerNum];
    [gametiles setObject:obj  forKey:newkey];
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    //NEW SYSTEM
    
    if([touches count] == 1)
    {
        //First Determine the location of the touch in one CGPoint
        CGPoint location;
        for (UITouch *touch in touches)
        {
            //Determine the location on the map that was touched
            location = [self convertTouchToNodeSpace: touch];
            
            //Convert the location into tile coordinates (20x20 map size)
            location.x=(int)(20*location.x/(self.tileMap.contentSize.width*self.tileMap.scaleX));
            location.y=(int)(20*location.y/(self.tileMap.contentSize.height*self.tileMap.scaleY));
            
            NSLog(@"Double Tap at coordinate x:%f y:%f", location.x, location.y);
        }
        NSString *locationkey = [NSString stringWithFormat:@"%d, %d", (int)location.x, (int)location.y];
        NSString* hqString = [NSString stringWithFormat:@"headquarters-%d", 3-[gameData getPlayerNum]];
        NSString* barrackString = [NSString stringWithFormat:@"barracks-%d", 3-[gameData getPlayerNum]];
        //Next determine if this was a double tap or a single tap
        NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
        NSTimeInterval diff = currentTime - timeTapBegins;
        if(diff < 0.2) //Selection Tap
        {
            //If it is not the player's turn, then double tap should show information on the tile tapped, but nothing else
            if(gameData.currentTurn != gameData.playerNumber)
            {
                if(unitSelected != nil)
                {
                    unitSelected = [self unitForLocationX:location.x Y:location.y];
                    xSelected = location.x;
                    ySelected = location.y;
                    if(unitSelected == nil)
                    {
                        //If the new selected unit still does not exist, update the UILayer and terminate the touch
                        [gameData displayUnit:nil];
                        return;
                    }
                    [gameData displayUnit:unitSelected];
                }
                return;
            }
            else
            {
                if(unitSelected != nil && (!([unitSelected isMovable]) && !([unitSelected getCanAttack])))
                {
                    //See if a new tile was selected
                    if((int)location.x != unitSelected.xloc || (int)location.y != unitSelected.yloc)
                    {
                        //A new tile was selected, so we should deselect the current unit and then act as if no unit was previously selected
                        //First deselect the current unit
                        unitSelected = nil;
                        [gameData displayUnit:nil];
                    }
                    else
                    {
                        //The unit was simply clicked again, do nothing
                        return;
                    }
                }
                
                //At this point, either the previously selected unit could do nothing and was cleaned up, the previously selected unit has an action to process, or there is no unit selected
                if(unitSelected == nil)
                {
                    //No unit is currently selected, so select the unit first
                    unitSelected = [self unitForLocationX:location.x Y:location.y];
                    xSelected = location.x;
                    ySelected = location.y;
                    if(unitSelected == nil)
                    {
                        //If the new selected unit still does not exist, then we can move to structure detection
                        [gameData displayUnit:nil];
                        //Find the building that was selected
                        Building *selectedBuilding = [self structureForLocationX:location.x Y:location.y];
                        if(selectedBuilding == nil)
                        {
                            return;
                        }
                        else
                        {
                            [gameData displayStructure:selectedBuilding];
                            
                            //Tentative, delete the commented section below
                            /*
                            //Determine What kind of action should happen based on what the structure is and who owns it
                            if(selectedBuilding.isOurBuilding)
                            {
                                
                            }
                            else
                            {
                                //This is not our building, just show stats
                                [gameData displayStructure:selectedBuilding];
                            }
                             */
                        }
                        return;
                    }
                    [gameData displayUnit:unitSelected];
                    
                    //Now determine if the unit is yours or the opponents (only shows data, then terminates)
                    if(!unitSelected.isOurUnit)
                    {
                        unitSelected = nil;
                        return;
                    }
                    
                    //Now determine if the unit is at its movement stage, attack stage, or finished stage
                    if([unitSelected isMovable])
                    {
                        //The unit can be moved, so display the movement range
                        [self showMovableRangeForUnitAtPoint: CGPointMake(location.x, location.y)];
                    }
                    else if([unitSelected getCanAttack])
                    {
                        //The unit can attack, so display the attack range
                        [self showAttackableRangeForUnitAtPoint:ccp(location.x, location.y)];
                        [self showCapturableRangeForUnitAtPoint:ccp(location.x, location.y)];
                        
                    }
                }
                else if([unitSelected isMovable])
                {
                    //The unit has been selected and is movable, which means that it's range is currently showing
                    //All we need to do is see if this touch is in the movable range
                    

                    if ([self isValidMove:location.x :location.y] && (abs(location.x-unitSelected.xloc)+abs(location.y-unitSelected.yloc) <= [unitSelected getMovement]) &&
                            (
                                [[gametiles objectForKey:locationkey] isEqualToString:@"empty"]
                            )
                        )
                    {
                        //Mark the selected unit as having already moved
                        [unitSelected hasMoved];
                        
                        //Clear the old location in the underlying 2D array
                        NSString *newkey = [NSString stringWithFormat:@"%d, %d", unitSelected.xloc, unitSelected.yloc];
                        NSString* obj = [NSString stringWithFormat:@"empty"];
                        [gametiles setObject:obj  forKey:newkey];
                        
                        //Move the unit and place it in its new space in the underlying 2d Array
                        newkey = [NSString stringWithFormat:@"%d, %d", (int)location.x, (int)location.y];
                        float xLocation = (location.x) * self.tileMap.tileSize.width * self.tileMap.scaleX/2;
                        float yLocation = location.y * self.tileMap.tileSize.height* self.tileMap.scaleY/2;
                        CCMoveTo *moveUnit = [CCMoveTo actionWithDuration:0.5 position:ccp(xLocation+20, yLocation+20)];
                        [unitSelected runAction:moveUnit];
                        unitSelected.xloc = location.x;
                        unitSelected.yloc = location.y;
                        
                        //Update the underlying 2D array with the unit's new location
                        obj = [NSString stringWithFormat:@"%@-%d", unitSelected.uType, [gameData getPlayerNum]];
                        [gametiles setObject:obj forKey:newkey];
                        [unitSelected setCanAttackTrue];
                        
                        //Clear the movement Tiles
                        for(CCSprite *spr in movementTiles)
                        {
                            [spr removeFromParentAndCleanup:YES];
                        }
                        
                        //Update the uiLayer to show the new location of the unit
                        [gameData displayUnit:unitSelected];
                        
                        //Now that the unit has moved, we should keep it selected and show it's attack range
                        [self showAttackableRangeForUnitAtPoint:ccp(location.x, location.y)];
                        //Now that the unit has moved, if this unit is over the headquarters, the unit now has the option to capture. If it captures, then the unit can no longer attack.
                        [self showCapturableRangeForUnitAtPoint:ccp(location.x, location.y)];
                        //TODO
                        
                        [self updateAnimation:unitSelected];
                        
                        return;
                    }
                    else
                    {
                        //Our attempt to move the unit was on an invalid tile, whether that was another unit or out of range
                        //Clear the movement Tiles
                        for(CCSprite *spr in movementTiles)
                        {
                            [spr removeFromParentAndCleanup:YES];
                        }
                        
                        //Deselect our unit, but save the space that we selected (in case structure stats are added to ui layer)
                        unitSelected = nil;
                        xSelected = location.x;
                        ySelected = location.y;
                        
                        //Update our UILayer to show that the unit has been deselected
                        [gameData displayUnit:nil];
                        
                        [self updateAnimation:unitSelected];
                        
                        return;
                    }
                }
                else if([unitSelected getCanAttack])
                {
                    //The unit has been selected and can attack, which means its attack range is already shown
                    //All we need to do here is determine if the attack is valid or not
                    NSString *locationkey = [NSString stringWithFormat:@"%d, %d", (int)location.x, (int)location.y];
                    if((abs(location.x-unitSelected.xloc)+abs(location.y-unitSelected.yloc) <= [unitSelected getAttackDistance])
                       && ([[gametiles objectForKey:locationkey] isEqualToString:[NSString stringWithFormat: @"bazooka-%d", 3-gameData.playerNumber]] || [[gametiles objectForKey:locationkey] isEqualToString:[NSString stringWithFormat: @"rifle-%d", 3-gameData.playerNumber]]))
                    {
                        //This is a valid attack, we should execute the attack and mark our unit as having attacked
                        [unitAttacking setCanAttackFalse];
                        
                        //Execute the actual attack
                        NSMutableArray * troops = [gameData getTroopsWithPlayerID: 3-[gameData getPlayerNum]];
                        //Find the soldier who is being attacked (we know this exists through our earlier if statement)
                        Unit * soldier;
                        for (int i = 0; i < [troops count]; i++)
                        {
                            if ([[troops objectAtIndex: i] getXPosition] == location.x
                                && [[troops objectAtIndex: i] getYPosition] == location.y)
                            {
                                soldier = [troops objectAtIndex: i];
                                break;
                            }
                        };
                        //Decrement the health of the soldier
                        int totaldefense = soldier.def;
                        NSString *buildinglocationkey = [NSString stringWithFormat: @"%d, %d", soldier.xloc, soldier.yloc];
                        NSString *buildinglocationObject = [buildingTiles2D objectForKey:buildinglocationkey];
                        if (buildinglocationObject == nil)
                        {
                            NSLog(@"Not a building");
                            totaldefense = soldier.def;
                        }
                        else{
                            NSLog(@"Is a building");
                            totaldefense +=2;
                        }
                        if (totaldefense > ((unitAttacking.health/5 + 1)*unitAttacking.att))
                        {
                            //if total defense > damage total done, then only lose one hp.
                            soldier.health--;
                        }
                        else{
                        soldier.health -= ((unitAttacking.health/5)+1)*unitAttacking.att - totaldefense;
                        }
                        if(soldier.health <= 0)
                        {
                            //If the attacked soldier has died, clean up after it
                            [gameData removeUnit:soldier];
                            [gametiles setObject:@"empty" forKey:locationkey];
                            [self removeChild:soldier cleanup:YES];
                        }
                        else
                        {
                            //If the attacked soldier is still alive, update its health label
                            [soldier setupAnim];
                        }
                        
                        //Clear the attacking Tiles
                        for(CCSprite *spr in attackableTiles)
                        {
                            [spr removeFromParentAndCleanup:YES];
                        }
                        for (CCSprite *spr in capturableTiles)
                        {
                            [spr removeFromParentAndCleanup:YES];
                        }
                        [self updateAnimation:unitSelected];
                        return;
                    }
                    else if(location.x == unitSelected.xloc && location.y == unitSelected.yloc && [self structureForLocationX:location.x Y:location.y] != nil)//We should add a check to see if a structure actually exists here so that it cleans up if it does not
                    {
                        //Here is where we do our capturing
                        Building *capturedBuilding = [self structureForLocationX:location.x Y:location.y];
                        
                        if (!capturedBuilding.isOurBuilding){
                             
                            bool tryCapture = [capturedBuilding capture:unitSelected.health/2];
                            if([capturedBuilding.structureType isEqualToString:@"headquarters"])
                            {
                                if(tryCapture)
                                {
                                    //Trigger win condition
                                    [gameData writeDataToFirebase];
                                    [[GameData GetGameData] playMenuMusic];
                                    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[[ResultsScene alloc] initWithCondition:true AndGameNum:gameData.gameNumber]]];
                                }
                                else
                                {
                                    HeadQuarters *h2 = (HeadQuarters*)capturedBuilding;
                                    [h2 setupAnim];
                                }
                            }
                            else
                            {
                                Barracks *b2 = (Barracks *)capturedBuilding;
                                [b2 setupAnim];
                                if(tryCapture)
                                {
                                    //Here is where we update our lists
                                    [gameData removeEnemyStructure:capturedBuilding];
                                    [buildingTiles2D setObject:[NSString stringWithFormat:@"barracks-%i",[gameData getPlayerNum]] forKey: locationkey];
                                }
                            }
                            [unitSelected setCanAttackFalse];
                            //Clear the attacking Tiles
                            for(CCSprite *spr in attackableTiles)
                            {
                                [spr removeFromParentAndCleanup:YES];
                            }
                            for (CCSprite *spr in capturableTiles)
                            {
                                [spr removeFromParentAndCleanup:YES];
                            }
                            
                        }
                    }
                    else
                    {
                        //The attack was invalid, we should clean up the attack range and deselect the unit
                        //Clear the attacking Tiles
                        for(CCSprite *spr in attackableTiles)
                        {
                            [spr removeFromParentAndCleanup:YES];
                        }
                        for (CCSprite *spr in capturableTiles)
                        {
                            [spr removeFromParentAndCleanup:YES];
                        }
                        //Deselect our unit, but save the space that we selected (in case structure stats are added to ui layer)
                        unitSelected = nil;
                        xSelected = location.x;
                        ySelected = location.y;
                        
                        //Update our UILayer to show that the unit has been deselected
                        [gameData displayUnit:nil];
                        
                        [self updateAnimation:unitSelected];                        return;
                    }
                }
            }
        }
        else //Held tap, so map dragging
        {
            bool outOfBounds = false;
            
            NSLog(@"x %f y %f ",self.position.x,self.position.y);
            
            CGPoint newPos=self.position;
            CGSize size = [CCDirector sharedDirector].winSize;
            
            if(self.position.x>0){
                NSLog(@"out of x boundaries %f",self.position.x);
                newPos.x=0;
                outOfBounds = true;
            }
            else if(self.position.x<-(self.tileMap.contentSize.width*self.tileMap.scaleX*self.scaleX)+size.width){
                NSLog(@"out of x boundaries %f",self.position.x);
                newPos.x=-(self.tileMap.contentSize.width*self.tileMap.scaleX*self.scaleY)+size.width;
                outOfBounds = true;
            }
            
            if(self.position.y<-(self.tileMap.contentSize.height*self.tileMap.scaleY*self.scaleY)+(size.height*7/8)){
                NSLog(@"out of x boundaries %f",self.position.x);
                newPos.y=-(self.tileMap.contentSize.width*self.tileMap.scaleY*self.scaleY)+(size.height*7/8);
                outOfBounds = true;
                
            }
            else if(self.position.y>size.height/3){
                NSLog(@"out of y boundaries %f",self.position.x);
                newPos.y=size.height/3;
                outOfBounds = true;
            }
            
            if(outOfBounds)
            {
                [self stopAllActions];
                [self runAction:[CCMoveTo actionWithDuration:.1f position:newPos]];
            }
        }
    }
}

-(void)updateAnimation:(Unit *)unit{
    
   
    
    
       if ([unit isKindOfClass:[BazookaSoldier class]]){
        BazookaSoldier *b = ((BazookaSoldier *) unit);
        
        [b setupAnim];
    }
    else if ([unit isKindOfClass:[RifleSoldier class]]){
        RifleSoldier *b = ((RifleSoldier *) unit);
        
        [b setupAnim];
    }
    
}
-(void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    
    if([touches count] ==2){
        NSLog(@"multi");
        
        NSArray *touchArray = [touches allObjects];
        UITouch *fingerOne = [touchArray objectAtIndex:0];
        UITouch *fingerTwo = [touchArray objectAtIndex:1];
        
        distance=[self calculate:fingerOne calculate2:fingerTwo];
        NSLog(@"distance final %f and distance initial  %f",distance,distanceInitial);
        
        float newScale=self.scaleX*(distance/distanceInitial);
        
        if (newScale>2)
        {
            newScale=2;
        }
        else if (newScale<.5)
        {
            newScale=.5;
        }
        
        NSLog(@"%f", newScale);
        
        [self setScaleX:newScale];
        [self setScaleY:newScale];
        
        [self.tileMap setPosition: ccp(0,0)];
        [self.background setPosition: ccp(0,0)];
        
        float tileWidth = self.tileMap.tileSize.width*self.scaleX;
        [GameData GetGameData].tileWidth = tileWidth;
        
        return;
    }
    if([touches count] ==1){
        NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
        if((currentTime-timeLastMoved) < .11f)
        {
            return;
        }
        timeLastMoved = currentTime;
        for(UITouch *touch in touches)
        {
            CGSize size = [CCDirector sharedDirector].winSize;
            /*if(clickedYPos<size.height/3 || clickedYPos>size.height*7/8)
             {
             return;
             }*/
            CGPoint location = [self convertTouchToNodeSpace: touch];
            CGPoint newPos = self.position;
            newPos.x+=(location.x-clickedXPos);
            newPos.y+=(location.y-clickedYPos);
            [self runAction:[CCMoveTo actionWithDuration:.1f position:newPos]];
            clickedXPos = location.x;
            clickedYPos = location.y;
            return;
        }
    }
}

-(float)calculate:(UITouch *)fingerOne
       calculate2:(UITouch *)fingerTwo{
    
    CGPoint location = [self convertTouchToNodeSpace: fingerOne];
    CGPoint location2 = [self convertTouchToNodeSpace: fingerTwo];
    
    float diffX=fabsf(location.x-location2.x);
    float diffY=fabsf(location.y-location2.y);
    
    float distancePoints= sqrtf(diffX*diffX+diffY*diffY);
    
    return distancePoints;
}

-(void) deleteObjectAtLocationAtPoint: (CGPoint) clickedPoint
{
    NSString *locationkey = [NSString stringWithFormat:@"%d, %d", (int)clickedPoint.x, (int)clickedPoint.y];
    [gametiles setObject: @"empty" forKey: locationkey];
    NSMutableArray * troops = [gameData getTroopsWithPlayerID: [gameData getPlayerNum]];
    
    for (int i = 0; i < [troops count]; i++)
    {
        NSLog(@"[deleteObjectAtLocationAtPoint] Clicked: (%f, %f), Position at index %d: (%d, %d)",
              clickedPoint.x,
              clickedPoint.y,
              i,
              [[troops objectAtIndex: i] getXPosition],
              [[troops objectAtIndex: i] getYPosition]
              );
        if ([[troops objectAtIndex: i] getXPosition] == clickedPoint.x
            && [[troops objectAtIndex: i] getYPosition] == clickedPoint.y)
        {
            [self removeChild: [troops objectAtIndex: i]];
        }
    }
}

-(bool) showMovableRangeForUnitAtPoint:(CGPoint) unitLocation
{
    
    
    NSMutableArray * troops = [gameData getTroopsWithPlayerID: [gameData getPlayerNum]];
    
    Unit * soldier;
    
    for (int i = 0; i < [troops count]; i++)
    {
        NSLog(@"[ShowMovableRangeForUnitAtPointClicked: (%d, %d), Position at index %d: (%d, %d)",
              (int)unitLocation.x,
              (int)unitLocation.y,
              i,
              [[troops objectAtIndex: i] getXPosition],
              [[troops objectAtIndex: i] getYPosition]
              );
        if ([[troops objectAtIndex: i] getXPosition] == unitLocation.x
            && [[troops objectAtIndex: i] getYPosition] == unitLocation.y)
        {
            soldier = [troops objectAtIndex: i];
        }
    }
    
    //Test if soldier is movable
    if(![soldier isMovable])
    {
        return false;
    }
    
    isMovingAnObject = true;
    unitMoving = soldier;
    
    int movementRange = [soldier getMovement];
    
    movementTiles = [NSMutableArray arrayWithObjects:nil];
    NSLog(@"UnitLocation X: %i Y:%i", (int)unitLocation.x, (int)unitLocation.y);
    for (int i = 0 ; i <= 19; i++)
    {
        for (int j = 0; j <= 19; j++)
        {
            NSString *locationkey = [NSString stringWithFormat:@"%d, %d", i, j];
            if ((abs(i - (int)unitLocation.x) + abs(j - (int)unitLocation.y)) <= movementRange &&
                (
                 [[gametiles objectForKey:locationkey] isEqualToString:@"empty"]
                 //||
                 //([[gametiles objectForKey:locationkey] rangeOfString:@"HQ-"].location != NSNotFound)
                 )
                && [self isValidMove:i :j])
            {
                NSLog(@"X: %i Y: %i", i, j);
                CCSprite *movementSprite = [CCSprite spriteWithFile:@"blue-square.png"];
                movementSprite.scaleX = self.tileMap.tileSize.width * self.tileMap.scaleX / movementSprite.contentSize.width;
                movementSprite.scaleX /= 2;
                movementSprite.scaleY = self.tileMap.tileSize.height * self.tileMap.scaleY / movementSprite.contentSize.height;
                movementSprite.scaleY /= 2;
                [movementSprite setPosition: ccp(
                                                 (int)((i + 1)*self.tileMap.tileSize.width*self.tileMap.scaleX/2 - movementSprite.contentSize.width*movementSprite.scaleX/2),
                                                 ((j + 1)*self.tileMap.tileSize.height*self.tileMap.scaleY/2 - movementSprite.contentSize.height*movementSprite.scaleY/2))];
                [self addChild: movementSprite z:-1];
                [movementSprite retain];
                [movementTiles addObject: movementSprite];
            }
        }
    }
    [movementTiles retain];
    return true;
}

-(void) showAttackableRangeForUnitAtPoint:(CGPoint) unitLocation
{
    
    NSMutableArray * troops = [gameData getTroopsWithPlayerID: [gameData getPlayerNum]];
    
    Unit * soldier;
    
    for (int i = 0; i < [troops count]; i++)
    {
        NSLog(@"Clicked: (%d, %d), Position at index %d: (%d, %d)",
              (int)unitLocation.x,
              (int)unitLocation.y,
              i,
              [[troops objectAtIndex: i] getXPosition],
              [[troops objectAtIndex: i] getYPosition]
              );
        if ([[troops objectAtIndex: i] getXPosition] == unitLocation.x
            && [[troops objectAtIndex: i] getYPosition] == unitLocation.y)
        {
            soldier = [troops objectAtIndex: i];
            [soldier getSprite].color = ccc3(100, 100, 100);
        }
    }
    
    //Test if soldier can Attack/Capture
    if(![soldier getCanAttack])
    {
        return;
    }
    
    isAttacking = true;
    unitAttacking = soldier;
    
    int attackRange = [soldier getAttackDistance];
    
    /*CCSprite *movementSprite = [CCSprite spriteWithFile:@"blue-square.png"];
     movementSprite.scaleX = self.tileMap.tileSize.width * self.tileMap.scaleX / movementSprite.contentSize.width;
     movementSprite.scaleX /= 2;
     movementSprite.scaleY = self.tileMap.tileSize.height * self.tileMap.scaleY / movementSprite.contentSize.height;
     movementSprite.scaleY /= 2;
     
     [self addChild: movementSprite];*/
    attackableTiles = [NSMutableArray arrayWithObjects:nil];
    NSLog(@"UnitLocation X: %i Y:%i", (int)unitLocation.x, (int)unitLocation.y);
    for (int i = 0 ; i <= 19; i++)
    {
        for (int j = 0; j <= 19; j++)
        {
            if ((abs(i - (int)unitLocation.x) + abs(j - (int)unitLocation.y)) <= attackRange && !(i==unitLocation.x && j==unitLocation.y))
            {
                NSLog(@"X: %i Y: %i", i, j);
                CCSprite *attackSprite = [CCSprite spriteWithFile:@"red-square.png"];
                attackSprite.scaleX = self.tileMap.tileSize.width * self.tileMap.scaleX / attackSprite.contentSize.width;
                attackSprite.scaleX /= 2;
                attackSprite.scaleY = self.tileMap.tileSize.height * self.tileMap.scaleY / attackSprite.contentSize.height;
                attackSprite.scaleY /= 2;
                [attackSprite setPosition: ccp(
                                               (int)((i + 1)*self.tileMap.tileSize.width*self.tileMap.scaleX/2 - attackSprite.contentSize.width*attackSprite.scaleX/2),
                                               ((j + 1)*self.tileMap.tileSize.height*self.tileMap.scaleY/2 - attackSprite.contentSize.height*attackSprite.scaleY/2))];
                [self addChild: attackSprite z:-1];
                [attackSprite retain];
                [attackableTiles addObject: attackSprite];
            }
        }
    }
    [attackableTiles retain];
    
}
-(void) showCapturableRangeForUnitAtPoint:(CGPoint) unitLocation
{
    
    
    NSMutableArray * troops = [gameData getTroopsWithPlayerID: [gameData getPlayerNum]];
    
    Unit * soldier;
    
    for (int i = 0; i < [troops count]; i++)
    {
        NSLog(@"Clicked: (%d, %d), Position at index %d: (%d, %d)",
              (int)unitLocation.x,
              (int)unitLocation.y,
              i,
              [[troops objectAtIndex: i] getXPosition],
              [[troops objectAtIndex: i] getYPosition]
              );
        if ([[troops objectAtIndex: i] getXPosition] == unitLocation.x
            && [[troops objectAtIndex: i] getYPosition] == unitLocation.y)
        {
            soldier = [troops objectAtIndex: i];
        }
    }
    capturableTiles = [NSMutableArray arrayWithObjects: nil];
    [capturableTiles retain];
    //Test if soldier can Attack
    if(![soldier getCanAttack])
    {
        return;
    }

    [self updateAnimation:soldier];
    NSString *locationkey = [NSString stringWithFormat:@"%d, %d", soldier.xloc, soldier.yloc];
    //NSLog(locationkey);
    
    NSString* hqString = [NSString stringWithFormat:@"headquarters-%d", 3-[gameData getPlayerNum]];
    NSString* barrackString = [NSString stringWithFormat:@"barracks-%d", 3-[gameData getPlayerNum]];
    //NSString* barrackString = @"barracks-%d";
    if ([[buildingTiles2D objectForKey:locationkey] isEqualToString:hqString] || [[buildingTiles2D objectForKey:locationkey] isEqualToString:barrackString])
    {
        CCSprite *captureSprite = [CCSprite spriteWithFile:@"blue-square.png"];
        captureSprite.scaleX = self.tileMap.tileSize.width * self.tileMap.scaleX / captureSprite.contentSize.width;
        captureSprite.scaleX /= 2;
        captureSprite.scaleY = self.tileMap.tileSize.height * self.tileMap.scaleY / captureSprite.contentSize.height;
        captureSprite.scaleY /= 2;
   
        [captureSprite setPosition: ccp(
                                        (int)((soldier.xloc+1)*self.tileMap.tileSize.height*self.tileMap.scaleY/2 - captureSprite.contentSize.height*captureSprite.scaleY/2),
                                         (int)((soldier.yloc+1)*self.tileMap.tileSize.height*self.tileMap.scaleY/2 - captureSprite.contentSize.height*captureSprite.scaleY/2)
                                        )];
        [self addChild: captureSprite z:-1];
        [captureSprite retain];
        [capturableTiles addObject: captureSprite];
        [capturableTiles retain];
    }
    else
        return;
}


-(void) moveObjectToLocationToPoint: (CGPoint) clickedPoint : (Unit*) moveUnit
{
    NSString *previouskey = [NSString stringWithFormat:@"%d, %d", (int) moveUnit.xloc, (int) moveUnit.yloc];
    NSString *locationkey = [NSString stringWithFormat:@"%d, %d", (int)clickedPoint.x, (int)clickedPoint.y];
    NSString *objectValue = [NSString stringWithFormat: @"%@-%d", [moveUnit getUnitType], [gameData getPlayerNum]];
    [gametiles setObject: @"empty" forKey: previouskey];
    [gametiles setObject: objectValue forKey: locationkey];
    NSMutableArray * troops = [gameData getTroopsWithPlayerID: 2];
    
    for (int i = 0; i < [troops count]; i++)
    {
        if ([[troops objectAtIndex: i] isEqual: moveUnit])
        {
            float xLocation = clickedPoint.x * self.tileMap.tileSize.width * self.tileMap.scaleX/2;
            float yLocation = clickedPoint.y * self.tileMap.tileSize.height* self.tileMap.scaleY/2;
            //((int)location.y %(int)(self.tileMap.scaleY*self.tileMap.tileSize.height*self.scaleY/20));
            [moveUnit setPosition:ccp(xLocation + 20, yLocation +  20)];
            moveUnit.xloc = clickedPoint.x;
            moveUnit.yloc = clickedPoint.y;
        }
    }

}

-(void) initializeStructures
{
    NSMutableArray *usArray = [NSMutableArray arrayWithObjects:nil];
    NSMutableArray *themArray = [NSMutableArray arrayWithObjects:nil];
    
    for (int i=0; i<_tileMap.mapSize.height; i++){
        for (int j=0; j<_tileMap.mapSize.width;j++){
            
            CGPoint tileCoord = ccp(i,abs(j-19));
            int tileGid = [_buildings tileGIDAt:tileCoord];
            if (tileGid)
            {
                NSDictionary *properties = [_tileMap propertiesForGID:tileGid];
                if (properties)
                {
                    NSString *collision = [properties valueForKey:@"player"];
                    //properties[@"Accesss"];
                    if (collision && [collision isEqualToString:@"1"])
                    {
                        NSString *name = [NSString stringWithFormat:@"%d,%d", i, j];
                        [name retain];
                        [usArray addObject:name];
                        
                    }else if (collision && [collision isEqualToString:@"2"])
                    {
                        NSString *name = [NSString stringWithFormat:@"%d,%d", i, j];
                        [name retain];
                        [themArray addObject:name];
                    }
                }
            }
        }
    }
    
    bool hasMadeHeadquarters = false;
    int count = 0;
    while(count < 5 && [usArray count] > 0)
    {
        int index = arc4random()%[usArray count];
        NSString *toBeParsed = [usArray objectAtIndex:index];
        NSArray *arr = [toBeParsed componentsSeparatedByString:@","];
        NSString *tileOne = [arr objectAtIndex:0];
        int xTile = [tileOne intValue];
        NSString *tileTwo = [arr objectAtIndex:1];
        int yTile = [tileTwo intValue];
        
        if(hasMadeHeadquarters)
        {
            Building *myBuilding = [[Barracks alloc] init];
            [myBuilding setOurBuilding:true];
            myBuilding.xloc = xTile;
            myBuilding.yloc = yTile;
            myBuilding.hqintegrity = 10;
            [myBuilding retain];
            [self addStructure:myBuilding];
            [gameData addOurStructure:myBuilding];
            [usArray removeObjectAtIndex:index];
            count++;
        }
        else
        {
            if((20-xTile) < 5 && yTile < 5){
                Building *myHQ = [[HeadQuarters alloc] init];
                [myHQ setOurBuilding:true];
                myHQ.xloc = xTile;
                myHQ.yloc = yTile;
                myHQ.hqintegrity = 10;
                [myHQ retain];
                [self addStructure:myHQ];
                [gameData addOurStructure:myHQ];
                hasMadeHeadquarters = true;
                [usArray removeObjectAtIndex:index];
                count++;
            }
        }
    }
    hasMadeHeadquarters = false;
    count = 0;
    while(count < 5 && [themArray count] > 0)
    {
        int index = arc4random()%[themArray count];
        NSString *toBeParsed = [themArray objectAtIndex:index];
        NSArray *arr = [toBeParsed componentsSeparatedByString:@","];
        NSString *tileOne = [arr objectAtIndex:0];
        int xTile = [tileOne intValue];
        NSString *tileTwo = [arr objectAtIndex:1];
        int yTile = [tileTwo intValue];
        
        if(hasMadeHeadquarters)
        {
            Building *enemyBuilding = [[Barracks alloc] init];
            [enemyBuilding setOurBuilding:false];
            enemyBuilding.xloc = xTile;
            enemyBuilding.yloc = yTile;
            enemyBuilding.hqintegrity = 10;
            [enemyBuilding retain];
            [self addStructure:enemyBuilding];
            [gameData addEnemyStructure:enemyBuilding];
            [themArray removeObjectAtIndex:index];
            count++;
        }
        else
        {
            if(xTile < 5 && (20-yTile)<5)
            {
                Building *enemyHQ = [[HeadQuarters alloc] init];
                [enemyHQ setOurBuilding:false];
                enemyHQ.xloc = xTile;
                enemyHQ.yloc = yTile;
                enemyHQ.hqintegrity = 10;
                [enemyHQ retain];
                [self addStructure:enemyHQ];
                [gameData addEnemyStructure:enemyHQ];
                hasMadeHeadquarters = true;
                [themArray removeObjectAtIndex:index];
                count++;
            }
        }
    }
}
@end
