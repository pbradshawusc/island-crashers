//
//  Barracks.h
//  Island Crashers
//
//  Created by ITP Student on 4/7/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCSprite.h"
#import "Building.h"
#import "cocos2d.h"
@interface Barracks : Building
-(void) attack: (int) cap;
-(void) reset;
@end
