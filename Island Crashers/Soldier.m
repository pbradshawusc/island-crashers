//
//  Soldier.m
//  Island Crashers
//
//  Created by Joseph Lin on 3/24/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "Soldier.h"

@implementation Soldier
- (id)init
{
    self = [super init];
    if (self) {
        soldier_action = @"walking";
        //CCSpriteBatchNode *sprite_batch;
        //sprite_batch = [CCSpriteBatchNode batchNodeWithFile: @"soldier-blue.plist"];
/*        CGSize size = [[CCDirector sharedDirector] winSize];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"soldier-blue.plist"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"soldier-blue.png"];
        
        
        NSMutableArray *walkAnimFrames = [NSMutableArray array];
        for (int i=1; i<=6; i++) {
            [walkAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"soldier-bazooka-running-blue-frame-0%d.png",i]]];
        }
        self->soldier_sprite = [CCSprite spriteWithSpriteFrameName:@"soldier-bazooka-running-blue-frame-01.png"];
        self->soldier_sprite.position = ccp(size.width/2, size.height/2);
        CCAnimation *walkAnim = [CCAnimation
                                 animationWithSpriteFrames:walkAnimFrames delay:0.1f];
        self->walkAction = [CCRepeatForever actionWithAction:
                            [CCAnimate actionWithAnimation:walkAnim]];
        [self->soldier_sprite runAction:self->walkAction];
        [spriteSheet addChild:self->soldier_sprite];
        [self addChild:spriteSheet];*/
        //[self addChild:sprite_batch];
        //CCSprite *sprite = [CCSprite spriteWithFile:@"soldier-bazooka-running-blue-frame-01"];
        //[sprite_batch addChild:sprite];
    }
    return self;
}
-(void) setShooting
{
    soldier_action = @"shooting";
    NSLog(@"HW5");
}
-(void) update:(ccTime)dt
{
    [self setupAnim];
}
-(int) baseFirepower
{
    //Returns base firepower to scene to be increased by multiplier
    return firepower;
}
-(int) baseDefense
{
    //Returns base defense to scene to be increased by multiplier
    return defense;
}
-(NSString*) type
{
    return soldier_action;
}
//@synthesize _type = soldier_action;
@end
