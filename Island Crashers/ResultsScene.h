//
//  ResultsScene.h
//  Island Crashers
//
//  Created by Alexa Rucks on 4/27/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCScene.h"
#import "cocos2d.h"

@interface ResultsScene : CCScene
{
    bool back;
    int gameNum;
}
-(id) initWithCondition:(bool)didWin AndGameNum:(int)g;
@end
