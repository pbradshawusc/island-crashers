//
//  IntroLayer.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/12/14.
//  Copyright iON Industries 2014. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "GameData.h"

// HelloWorldLayer
@interface IntroLayer : CCLayer
{
    GameData *data;
    double timePassed;
    
    bool clicked;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
