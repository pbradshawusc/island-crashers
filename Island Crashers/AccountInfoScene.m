//
//  AccountInfoScene.m
//  Island Crashers
//
//  Created by Alexa Rucks on 4/25/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "AccountInfoScene.h"
#import "TestFirebaseLayer.h"
#import "LoginScene.h"
@class MainMenuScene;

@implementation AccountInfoScene
-(id) init
{
    self = [super init];
    back = false;
    
    //Initialize the gameData
    data = [GameData GetGameData];
    
    // ask director for the window size
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    //Set the background for the layer
    CCSprite *background = [CCSprite spriteWithFile:@"SubMenu.png"];
    background.position = ccp(size.width/2, size.height/2);
    background.scaleY = 0.95;
    [self addChild: background];
    
    //Add a title
    CCLabelTTF *titleLabel = [CCLabelTTF labelWithString:@"Account Information" fontName:@"Marker Felt" fontSize:34];
    [titleLabel setPosition:ccp(size.width/2, size.height*7/9)];
    [titleLabel setColor: ccc3(0,0,0)];
    [self addChild:titleLabel];
    
    //Now lets add other labels for relevant information
    NSString *emailString = [NSString stringWithFormat:@"Email: %@", data.email];
    CCLabelTTF *emailLabel = [CCLabelTTF labelWithString:emailString fontName:@"Marker Felt" fontSize:24];
    [emailLabel setAnchorPoint:ccp(0,0)];
    [emailLabel setPosition:ccp(10, size.height*5/9)];
    [emailLabel setColor: ccc3(0,0,0)];
    [self addChild:emailLabel];
    
    NSString *experienceString = [NSString stringWithFormat:@"Experience: %@", data.exp];
    CCLabelTTF *experienceLabel = [CCLabelTTF labelWithString:experienceString fontName:@"Marker Felt" fontSize:24];
    [experienceLabel setAnchorPoint:ccp(0,0)];
    [experienceLabel setPosition:ccp(10, size.height*5/9 - emailLabel.contentSize.height)];
    [experienceLabel setColor: ccc3(0,0,0)];
    [self addChild:experienceLabel];
    
    //Finally lets add a couple of menu items for things a user might wish to do
    CCMenuItem *itemTestFirebaseLogin = [CCMenuItemFont itemWithString:@"Test Firebase Behavior" block:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[TestFirebaseLayer scene] ]];
    }];
    CCMenuItem *itemTestLogin = [CCMenuItemFont itemWithString:@"Log Out" block:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[LoginScene scene] ]];
    }];
    CCMenu *menu = [CCMenu menuWithItems:itemTestFirebaseLogin, itemTestLogin, nil];
    [menu alignItemsVerticallyWithPadding:20];
    [menu setPosition:ccp( size.width/2, size.height/3 - 50)];
    
    // Add the menu to the layer
    [self addChild:menu];
    [menu setColor: ccc3(0,0,0)];
    
    //We also need to be able to return to the main menu
    CCMenuItem *itemBack = [CCMenuItemFont itemWithString:@"<Back" block:^(id sender) {
        if(back)
        {
            return;
        }
        back = true;
        [[CCDirector sharedDirector] replaceScene:[CCTransitionPageTurn transitionWithDuration:1.0 scene:[MainMenuScene node] backwards:YES]];
    }];
    
    CCMenu *backMenu = [CCMenu menuWithItems:itemBack, nil];
    [backMenu setColor:ccc3(0, 0, 0)];
    [backMenu setPosition:ccp( 3+itemBack.contentSize.width/2, size.height*16/18 + 10)];
    [self addChild:backMenu];
    
    return self;
}
@end
