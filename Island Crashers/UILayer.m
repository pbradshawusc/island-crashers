//
//  UILayer.m
//  Island Crashers
//
//  Created by Alexa Rucks on 3/28/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "UILayer.h"
#import "AppDelegate.h"
#import "Unit.h"
#import "BazookaSoldier.h"
#import "RifleSoldier.h"
#import "GamePlayData.h"
@class GameListScene;
@class GamePlayScene;
@class GameData;

@implementation UILayer
-(id) init
{
    if(self = [super init])
    {
        //Initialize all unit information structures to nil
        unitLocation = nil;
        
        back = false;
        CGSize size = [CCDirector sharedDirector].winSize;
        CCSprite *spr_info = [CCSprite spriteWithFile:@"SPEECH_BOX.png" rect:CGRectMake(5,1,40,25)];
        spr_info.scaleX = size.width/spr_info.contentSize.width;
        spr_info.scaleY = size.height/(spr_info.contentSize.height*3);
        spr_info.position = ccp(spr_info.scaleX*spr_info.contentSize.width/2, spr_info.scaleY*spr_info.contentSize.height/2);
        [self addChild:spr_info];
        
        /*CCSprite *spr_info2 = [CCSprite spriteWithFile:@"SPEECH_BOX.png" rect:CGRectMake(5,28,40,25)];
        spr_info2.scaleX = 2*size.width/(spr_info2.contentSize.width*3);
        spr_info2.scaleY = size.height/(spr_info2.contentSize.height*3);
        spr_info2.position = ccp(size.width/3+(spr_info2.scaleX*spr_info2.contentSize.width/2), spr_info2.scaleY*spr_info2.contentSize.height/2);
        [self addChild:spr_info2 z:10];*/

        CCSprite *spr_nav = [CCSprite spriteWithFile:@"SPEECH_BOX.png" rect:CGRectMake(5,1,40,25)];
        spr_nav.scaleX = size.width/spr_nav.contentSize.width;
        spr_nav.scaleY = size.height/(spr_nav.contentSize.height*8);
        spr_nav.position = ccp(spr_nav.scaleX*spr_nav.contentSize.width/2, size.height-(spr_nav.scaleY*spr_nav.contentSize.height/2));
        [self addChild:spr_nav];
        
        
        
        
        CCMenuItem *itemBack = [CCMenuItemFont itemWithString:@"<Back" block:^(id sender) {
            back = true;
            //[gpd writeDataToFirebase];
            [self clean];
            [[GameData GetGameData] playMenuMusic];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameListScene node] ]];
        }];
        
        CCMenu *menu = [CCMenu menuWithItems:itemBack, nil];
        
        //TEST REFRESH CODE
        /*CCMenuItem *itemRefresh = [CCMenuItemFont itemWithString:@"Refresh" block:^(id sender) {
            [self refreshScene];
        }];
        [menu addChild: itemRefresh];
        [menu alignItemsVertically];*/
        //END REFRESH TEST
        
        //[menu alignItemsHorizontallyWithPadding:size.width/3];
        
        
        [menu setColor:ccc3(0, 0, 0)];
        [menu setPosition:ccp( 3+itemBack.contentSize.width/2, size.height*15/16)];
        [self addChild:menu];
    }
    
    return self;
    
}
-(void)update:(ccTime)delta{
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval diff = currentTime - timeBlinkLabel;
    if(diff > 3){
        [self removeChild:label];
        [self removeChild:blinkingLabel];
    
    [self unscheduleUpdate];
    }
}


-(void) clean
{
    [self removeChild:resourceManagerWrapper cleanup:YES];
    resourceManagerWrapper = nil;
}
-(void) DisplayUnit:(Unit *)u
{
    //First clean up all unit information structures if they are currently in use
    if(unitLocation != nil)
    {
        [self removeChild:unitLocation];
        unitLocation = nil;
    }
    if(unitMovementRange != nil)
    {
        [self removeChild:unitMovementRange];
        unitMovementRange = nil;
    }
    if(unitAttackRange != nil)
    {
        [self removeChild:unitAttackRange];
        unitAttackRange = nil;
    }
    if(unitFirepower != nil)
    {
        [self removeChild:unitFirepower];
        unitFirepower = nil;
    }
    if(unitDefense != nil)
    {
        [self removeChild:unitDefense];
        unitDefense = nil;
    }
    if(unitHealth != nil)
    {
        [self removeChild:unitHealth];
        unitHealth = nil;
    }
    if(unitImage != nil)
    {
        [self removeChild:unitImage];
        unitImage = nil;
    }
    if(unitStore != nil)
    {
        [self removeChild:unitStore];
        unitStore = nil;
    }
    
    //Check if the new unit is nil, if so then don't add new information structures
    if(u == nil)
    {
        return;
    }
    
    //Initialize and add new unit information structures
    CGSize size = [CCDirector sharedDirector].winSize;
    NSString *text = [NSString stringWithFormat:@"Coordinates X:%d, Y:%d", u.xloc, u.yloc];
    unitLocation = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:16];
    [self addChild:unitLocation];
    [unitLocation setPosition:ccp(size.width/2, size.height/3-2*unitLocation.contentSize.height)];
    [unitLocation setColor:ccc3(0,0,0)];
    
    text = [NSString stringWithFormat:@"Movement Range: %d", [u getMovement]];
    unitMovementRange = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:16];
    [self addChild:unitMovementRange];
    [unitMovementRange setPosition:ccp(size.width/2, size.height/3-3*unitLocation.contentSize.height)]; //FIX ME
    [unitMovementRange setColor:ccc3(0,0,0)];
    
    text = [NSString stringWithFormat:@"Attack Range: %d", [u getAttackDistance]];
    unitAttackRange = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:16];
    [self addChild:unitAttackRange];
    [unitAttackRange setPosition:ccp(size.width/2, size.height/3-4*unitLocation.contentSize.height)]; //FIX ME
    [unitAttackRange setColor:ccc3(0,0,0)];
    
    text = [NSString stringWithFormat:@"Firepower: %d", u.att];
    unitFirepower = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:16];
    [self addChild:unitFirepower];
    [unitFirepower setPosition:ccp(size.width/2, size.height/3-5*unitLocation.contentSize.height)]; //FIX ME
    [unitFirepower setColor:ccc3(0,0,0)];
    
    text = [NSString stringWithFormat:@"Defense: %d", u.def];
    unitDefense = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:16];
    [self addChild:unitDefense];
    [unitDefense setPosition:ccp(size.width/2, size.height/3-6*unitLocation.contentSize.height)]; //FIX ME
    [unitDefense setColor:ccc3(0,0,0)];
    
    text = [NSString stringWithFormat:@"Health: %d/10", u.health];
    unitHealth = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:16];
    [self addChild:unitHealth];
    [unitHealth setPosition:ccp(size.width/2, size.height/3-7*unitLocation.contentSize.height)]; //FIX ME
    [unitHealth setColor:ccc3(0,0,0)];
    
    unitImage = [[CCSprite alloc] init];
    [unitImage setScale:3];
    [unitImage runAction:[u.walkAct copy]];
    [self addChild:unitImage];
    [unitImage setPosition:ccp(size.width/5, size.height/6)];
    
    
}

-(void) DisplayStructure:(Building *)b
{
    //First clean up all unit information structures if they are currently in use
    displayedBuilding = b;
    if(unitLocation != nil)
    {
        [self removeChild:unitLocation];
        unitLocation = nil;
    }
    if(unitMovementRange != nil)
    {
        [self removeChild:unitMovementRange];
        unitMovementRange = nil;
    }
    if(unitAttackRange != nil)
    {
        [self removeChild:unitAttackRange];
        unitAttackRange = nil;
    }
    if(unitFirepower != nil)
    {
        [self removeChild:unitFirepower];
        unitFirepower = nil;
    }
    if(unitDefense != nil)
    {
        [self removeChild:unitDefense];
        unitDefense = nil;
    }
    if(unitHealth != nil)
    {
        [self removeChild:unitHealth];
        unitHealth = nil;
    }
    if(unitImage != nil)
    {
        [self removeChild:unitImage];
        unitImage = nil;
    }
    if(unitStore != nil)
    {
        [self removeChild:unitStore];
        unitStore = nil;
    }
    
    //Check if the new unit is nil, if so then don't add new information structures
    if(b == nil)
    {
        return;
    }
    
    //Initialize and add new unit information structures
    CGSize size = [CCDirector sharedDirector].winSize;
    NSString *text = [NSString stringWithFormat:@"Coordinates X:%d, Y:%d", b.xloc, b.yloc];
    unitLocation = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:16];
    [self addChild:unitLocation];
    [unitLocation setPosition:ccp(size.width/2, size.height/3-2*unitLocation.contentSize.height)];
    [unitLocation setColor:ccc3(0,0,0)];
    
    text = [NSString stringWithFormat:@"Defense Bonus: %d", 2];
    unitDefense = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:16];
    [self addChild:unitDefense];
    [unitDefense setPosition:ccp(size.width/2, size.height/3-3*unitLocation.contentSize.height)]; //FIX ME
    [unitDefense setColor:ccc3(0,0,0)];
    
    text = [NSString stringWithFormat:@"Integrity: %d/10", b.hqintegrity];
    unitHealth = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:16];
    [self addChild:unitHealth];
    [unitHealth setPosition:ccp(size.width/2, size.height/3-4*unitLocation.contentSize.height)]; //FIX ME
    [unitHealth setColor:ccc3(0,0,0)];
    
    if([b.structureType isEqualToString:@"barracks"] && b.isOurBuilding)
    {
        CCMenuItem *itemBazooka = [CCMenuItemFont itemWithString:@"Bazooka Soldier - $500" block:^(id sender) {
            UILayer *selfCapture = self;
            if(gpd.currentMoney < 500)
            {
                return;
            }
            gpd.currentMoney -= 500;
            Unit* u = [[[BazookaSoldier alloc]init]retain];
            u.isOurUnit = true;
            u.health = 10;
            u.xloc = b.xloc;
            u.yloc = b.yloc;
            [gpd addOurUnit:u];
            [map addUnit:u];
            [u hasMoved];
            [u setCanAttackFalse];
            [selfCapture DisplayUnit:u];
            [selfCapture updateMoneyLabel];
            [u setupAnim];
        }];
        CCMenuItem *itemRifle = [CCMenuItemFont itemWithString:@"Rifle Soldier - $300" block:^(id sender) {
            UILayer *selfCapture = self;
            if(gpd.currentMoney < 300)
            {
                return;
            }
            gpd.currentMoney -= 300;
            Unit* u = [[[RifleSoldier alloc]init]retain];
            u.isOurUnit = true;
            u.health = 10;
            u.xloc = b.xloc;
            u.yloc = b.yloc;
            [gpd addOurUnit:u];
            [map addUnit:u];
            [u hasMoved];
            [u setCanAttackFalse];
            [selfCapture DisplayUnit:u];
            [selfCapture updateMoneyLabel];
            [u setupAnim];
        }];
        unitStore = [CCMenu menuWithItems:itemBazooka, itemRifle, nil];
        [unitStore alignItemsVerticallyWithPadding:2];
        [unitStore setColor:ccc3(0, 0, 0)];
        [unitStore setPosition:ccp( size.width/2, size.height/8)];
        [self addChild:unitStore];
    }
    
}

-(void) setResourceManagerWithTroops: (NSMutableArray *) troops /*andStructures: (NSMutableArray *) structures andResources: (NSMutableArray *) resources*/
{
    /*if(back)
    {
        return;
    }
    //[self clean];
    if ([[self children] containsObject:resourceManagerWrapper]) {
        [self clean];
    }
    
    NSMutableArray *structureImages = [[NSMutableArray alloc] init];
    NSLog(@"TROOP COUNT: %lu", (unsigned long)troops.count);
    
    for (int i = 0; i < troops.count; i++)
    {
        if ([[[troops objectAtIndex: i] getUnitType] isEqualToString: @"bazooka"])
        {
            [structureImages addObject:[UIImage imageNamed: @"soldier-bazooka-running-blue-frame-01.png"]];
        }
        else if ([[[troops objectAtIndex: i] getUnitType] isEqualToString: @"rifle"])
        {
            [structureImages addObject:[UIImage imageNamed: @"soldier-blue-frame-02.png"]];
        }
//        UIImage *troopImage = [self renderUIImageFromSprite: [[troops objectAtIndex: i] getUnitSprite]];
//        [structureImages addObject: troopImage];
    }
    CGSize size = [CCDirector sharedDirector].winSize;
//    [structureImages addObject:[UIImage imageNamed: @"barakGreen.png" ]];
//    [structureImages addObject:[UIImage imageNamed: @"barakRed.png" ]];
//    [structureImages addObject:[UIImage imageNamed: @"buildingBlue.png" ]];
//    [structureImages addObject:[UIImage imageNamed: @"buildingGreen.png" ]];
//    [structureImages addObject:[UIImage imageNamed: @"civilianGreen.png" ]];
    
    structuresScrollPicker = [[InfiniteScrollPicker alloc] initWithFrame:CGRectMake(0, 13*size.height/18, size.width/3, 2*size.height/9)];
    [structuresScrollPicker setImageAry:structureImages];
    [structuresScrollPicker setHeightOffset:20];
    [structuresScrollPicker setPositionRatio:1.0];
    [structuresScrollPicker setAlphaOfobjs:0.3];
    [structuresScrollPicker setSelectedItem:0];
    [structuresScrollPicker setShowsHorizontalScrollIndicator:YES];
    [structuresScrollPicker setCenter:ccp(size.width/6,((2*13*size.height/18)+(2*size.height/9))/2)];
    resourceManagerWrapper = [CCUIViewWrapper wrapperForUIView:structuresScrollPicker];
    [self addChild:resourceManagerWrapper z:3];*/
}

/*
 * @source: http://www.cocos2d-iphone.org/forums/topic/convert-ccspriteframe-to-uiimage-is-this-possible/
 */
-(UIImage *) renderUIImageFromSprite :(CCSprite *)sprite {
    
    int tx = sprite.contentSize.width;
    int ty = sprite.contentSize.height;
    
    CCRenderTexture *renderer = [CCRenderTexture renderTextureWithWidth:tx height:ty];
    
    sprite.anchorPoint	= CGPointZero;
    
    [renderer begin];
    [sprite visit];
    [renderer end];
    
    return [renderer getUIImage];
}

-(void) setGamePlayData:(GamePlayData *)g
{
    gpd = g;
}
-(void) showEndTurn
{
    if(gpd.playerNumber == gpd.currentTurn)
    {
        CGSize size = [CCDirector sharedDirector].winSize;
        CCMenu *menu = [CCMenu menuWithItems:nil];
        itemEndTurn = [CCMenuItemFont itemWithString:@"End Turn" block:^(id sender) {
            gpd.currentTurn = 3-gpd.playerNumber;
            [gpd createFunds];
            [gpd writeDataToFirebase];
            [menu removeChild:itemEndTurn];
            //Run any methods to clean up gameplay scene if needed
        }];
        [menu addChild:itemEndTurn];
        //[menu alignItemsHorizontallyWithPadding:size.width/3];
        [menu setColor:ccc3(0, 0, 0)];
        [menu setPosition:ccp( size.width-3-itemEndTurn.contentSize.width/2, size.height*15/16)];
        [self addChild:menu];
        
        label = [CCSprite spriteWithFile:@"SPEECH_BOX.png" rect:CGRectMake(5,1,40,25)];
        label.scaleX = size.width/label.contentSize.width;
        label.scaleY = size.height/(label.contentSize.height*8);
        label.position = ccp(label.scaleX*label.contentSize.width/2, size.height/2);
        NSString *text = @"Your Turn!!!";
        blinkingLabel = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:34];
        [self addChild:label];
        [self addChild:blinkingLabel];
        [blinkingLabel setPosition:ccp(size.width/2, size.height/2)];
        [blinkingLabel setColor:ccc3(0,0,0)];
        
        timeBlinkLabel=[NSDate timeIntervalSinceReferenceDate];
        [self scheduleUpdate];
    }
}
-(void) refreshScene
{
    int num = gpd.gameNumber;
    int pn = gpd.playerNumber;
    [self clean];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.2 scene:[CCScene node] ]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.3 scene:[[GamePlayScene alloc] initWithGameNum:num PlayerNum:pn OpName:opName]]];
}



- (void)infiniteScrollPicker:(InfiniteScrollPicker *)infiniteScrollPicker didSelectAtImage:(UIImage *)image
{
    NSLog(@"Arrived at selector");
}

-(void) updateMoneyLabel
{
    if(moneyLabel != nil)
    {
        [self removeChild:moneyLabel];
    }
    CGSize size = [CCDirector sharedDirector].winSize;
    NSString *moneyText = [NSString stringWithFormat:@"$%d", gpd.currentMoney];
    moneyLabel = [CCLabelTTF labelWithString:moneyText fontName:@"Marker Felt" fontSize:24];
    [self addChild:moneyLabel];
    [moneyLabel setPosition:ccp(size.width/2, size.height*12/13)];
    [moneyLabel setColor:ccc3(0,0,0)];
}

-(void) setMapLayer:(MapLayer *)m
{
    map = m;
}

-(void) showOpponent:(NSString *)op
{
    opName = op;
    [opName retain];
    CGSize size = [CCDirector sharedDirector].winSize;
    NSString *opponentText = [NSString stringWithFormat:@"vs. %@", op];
    CCLabelTTF *opponentLabel = [CCLabelTTF labelWithString:opponentText fontName:@"Marker Felt" fontSize:16];
    [self addChild:opponentLabel];
    [opponentLabel setPosition:ccp(size.width/2, opponentLabel.contentSize.height/2+8)];
    [opponentLabel setColor:ccc3(0,0,0)];
}
@end
