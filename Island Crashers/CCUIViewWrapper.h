//
//  CCUIViewWrapper.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/16/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "cocos2d.h"

@interface CCUIViewWrapper : CCSprite
{
    UIView *uiItem;
    float rotation;
}

@property (nonatomic, retain) UIView *uiItem;

+ (id) wrapperForUIView:(UIView*)ui;
- (id) initForUIView:(UIView*)ui;

- (void) updateUIViewTransform;

@end