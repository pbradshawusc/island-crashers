//
//  GameData.m
//  Island Crashers
//
//  Created by Alexa Rucks on 3/15/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "GameData.h"
#import "SimpleAudioEngine.h"
GameData *data = nil;
@implementation GameData
@synthesize email = currentEmail;
@synthesize password = currentPassword;
@synthesize uid = uniqueID;
@synthesize games = currentGames;
@synthesize tileWidth = tWidth;
@synthesize exp = experience;
+(GameData *) GetGameData
{
    if(data == nil){
        data = [[GameData alloc] init];
    }
    return data;
}
-(id) init
{
    self = [super init];
    
    //Set up the Firebase Link and Authorization Client
    f = [[Firebase alloc] initWithUrl:@"https://dazzling-fire-1374.firebaseio.com/"];
    authClient = [[FirebaseSimpleLogin alloc] initWithRef:f];
    [authClient logout];
    
    //See if any email is in memory
    currentEmail = [[NSUserDefaults standardUserDefaults] stringForKey:@"IslandCrashersEmail"];
    if(currentEmail != nil)
    {
        [currentEmail retain];
    }
    //NSLog(currentEmail);
    currentPassword = [[NSUserDefaults standardUserDefaults] stringForKey:@"IslandCrashersPassword"];
    if(currentPassword != nil)
    {
        [currentPassword retain];
    }
    //NSLog(currentPassword);
    experience = [NSNumber numberWithInt:0];
    [experience retain];
    currentGames = [NSString stringWithFormat:@"none"];
    [currentGames retain];
    
    isLoggedIn = NO;
    
    return self;
}
-(BOOL) logIn
{
    isLoggedIn = NO;
    if(currentEmail == nil || currentPassword == nil)
    {
        isLoggedIn = NO;
        return NO;
    }
    [authClient loginWithEmail:currentEmail andPassword:currentPassword
           withCompletionBlock:^(NSError* error, FAUser* user) {
               
               if (error != nil) {
                   // There was an error logging in to this account
                   NSLog(@"%@",error);
                   NSLog(@"%@, %@", currentEmail, currentPassword);
                   isLoggedIn = NO;
                   return;
               } else {
                   // We are now logged in
                   NSLog(@"Logged in with: %@", [user uid]);
                   uniqueID = [NSString stringWithString:[[user uid] stringByAppendingString:@""]];
                   [uniqueID retain];
                   [[NSUserDefaults standardUserDefaults] setObject:currentEmail forKey:@"IslandCrashersEmail"];
                   NSLog(currentEmail);
                   [[NSUserDefaults standardUserDefaults] setObject:currentPassword forKey:@"IslandCrashersPassword"];
                   NSLog(currentPassword);
                   
                   NSString *filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/users/%@",uniqueID];
                   Firebase* f = [[Firebase alloc] initWithUrl:filepath];
                   
                   [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
                    {
                        isLoggedIn = YES;
                        bool changed = false;
                        for(FDataSnapshot* nse in [snapshot children])
                        {
                            if([nse.name isEqualToString:@"experience"])
                            {
                                if(experience != nse.value)
                                {
                                    experience = [NSNumber numberWithInt:[nse.value intValue]];
                                    [experience retain];
                                    changed = true;
                                }
                            }
                            else if([nse.name isEqualToString:@"currentGames"])
                            {
                                if(currentGames != nse.value)
                                {
                                    currentGames = [NSString stringWithFormat:nse.value];
                                    [currentGames retain];
                                    changed = true;
                                }
                            }
                        }
                        //[self saveUserData];
                        if(changed)
                        {
                            //[self saveUserData];
                        }
                    }];
                   //isLoggedIn = YES;
                   
               }
           }];
    return isLoggedIn;
}
-(void) logOut
{
    [authClient logout];
    isLoggedIn = NO;
}
-(BOOL) checkLoginStatus
{
    //Check login status
    [authClient checkAuthStatusWithBlock:^(NSError* error, FAUser* user) {
        if (error != nil) {
            // Oh no! There was an error performing the check
            NSLog(@"ErrorCheckingStatus");
            isLoggedIn = NO;
        } else if (user == nil) {
            // No user is logged in
            NSLog(@"No User Checking Status");
            isLoggedIn = NO;
        } else {
            if(![user.email isEqualToString:currentEmail])
            {
                isLoggedIn = NO;
                return;
            }
            // There is a logged in user
            NSLog(@"Logged In Checking Status User: %@", [user uid]);
            uniqueID = [NSString stringWithString:[user uid]];
            [uniqueID retain];
            isLoggedIn = YES;
        }
    }];
    return isLoggedIn;
}
-(BOOL) createAccountWithEmail:(NSString *)em Password:(NSString *)pas
{
    __block int finished = -1;
    currentEmail = [em copy];
    [currentEmail retain];
    currentPassword = [pas copy];
    [currentPassword retain];
    [authClient createUserWithEmail:em password:pas andCompletionBlock:^(NSError* error, FAUser* user) {
        if (error != nil)
        {
            finished = 1;
        }
        else
        {
            finished = 0;
            currentGames = [NSString stringWithFormat:@"none"];
            [currentGames retain];
            uniqueID = [NSString stringWithFormat:@"%@", [user uid]];
            [uniqueID retain];
            experience = [NSNumber numberWithInt:0];
            [experience retain];
            NSString *path = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/users/%@", uniqueID];
            Firebase *f2 = [[Firebase alloc] initWithUrl:path];
            [f2 setValue:@{@"email" : currentEmail, @"password": currentPassword, @"currentGames" : currentGames, @"experience" : experience}];
            [self logIn];
        }
    }];
    return (finished == 0);
}
-(void) saveUserData
{
    if(isLoggedIn && uniqueID != nil && [uniqueID class] == [NSString class])
    {
        NSString *path = [NSString stringWithFormat:@"users/%@", uniqueID];
        [[f childByAppendingPath:path] setValue:@{@"email" : currentEmail, @"password": currentPassword, @"currentGames" : currentGames, @"experience" : experience}];
        [[NSUserDefaults standardUserDefaults] setObject:currentPassword forKey:@"IslandCrashersPassword"];
        [[NSUserDefaults standardUserDefaults] setObject:currentEmail forKey:@"IslandCrashersPassword"];
    }
    else if(uniqueID == nil || [uniqueID class] != [NSString class])
    {
        NSLog(@"UniqueID nil");
    }
}
-(Firebase *) getFirebaseRoot
{
    return f;
}
-(void) playMenuMusic
{
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"8-Bit -Don't Worry Be Happy- -Bobby McFerrin.mp3" loop:YES];
}
-(void) playGameMusic
{
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Take On Me (a-ha) 8 bit Remix.mp3" loop:YES];
}
@end
