//
//  GameListScene.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/31/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"

@interface GameListScene : CCSprite
{
    NSString *opponentName;
    NSString *opponentUID;
    int num, pn;
    bool hitNewGame, newGameCreated, updatedOne, updatedTwo;
}

@end
