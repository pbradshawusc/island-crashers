//
//  GamePlayData.h
//  Island Crashers
//
//  Created by Joseph Lin on 4/13/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Unit.h"
#import "Building.h"
#import "UILayer.h"

@interface GamePlayData : NSObject
{
    NSMutableArray* OurUnitArray;
    NSMutableArray* EnemyUnitArray;
    NSMutableArray* OurStructureArray;
    NSMutableArray* EnemyStructureArray;
    
    int gameNum;
    int myPlayerNum;
    int currentTurnNum;
    int myMaterials;
    int myFood;
    int myMoney;
    int myDefenseBonus;
    int winner;
    
    UILayer * uiLayer;
}
@property int gameNumber;
@property int playerNumber;
@property int currentTurn;
@property int currentMaterials;
@property int currentFood;
@property int currentMoney;
@property int defenseBonus;
@property int theWinner;
-(void) addOurUnit: (Unit*) NewUnit;
-(void) addEnemyUnit: (Unit*) NewUnit;
-(void) addOurStructure: (Building *) NewBuilding;
-(void) addEnemyStructure: (Building *) NewBuilding;
-(void) removeEnemyStructure: (Building *) OldBuilding;
-(void) removeUnit: (Unit*) DeadUnit;
-(NSMutableArray *) getTroopsWithPlayerID: (int) playerNum;
-(NSMutableArray *) getStructuresWithPlayerID: (int) playerNum;
-(void) writeDataToFirebase;
-(int) getPlayerNum;
-(void) displayUnit:(Unit*)u;
-(void) displayStructure:(Building*)b;
- (id)initWithUILayer: (UILayer *) ui;
-(void) createFunds;
@end
