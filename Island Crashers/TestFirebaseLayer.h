//
//  HelloWorldLayer.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/12/14.
//  Copyright iON Industries 2014. All rights reserved.
//


#import <GameKit/GameKit.h>
#import "Soldier.h"

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "GameData.h"

// HelloWorldLayer
@interface TestFirebaseLayer : CCLayer <GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>
{
    CCLabelTTF *titleLabel;
    GameData *data;

}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
