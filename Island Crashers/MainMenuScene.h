//
//  MainMenuScene.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/15/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCScene.h"
#import "cocos2d.h"
#import "GameData.h"

@interface MainMenuScene : CCScene
{
    GameData *data;
}
@end
