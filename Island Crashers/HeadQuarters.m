//
//  HeadQuarters.m
//  Island Crashers
//
//  Created by ITP Student on 3/31/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "HeadQuarters.h"

@implementation HeadQuarters
- (id)init
{
    self = [super init];
    if (self) {
        type = [NSString stringWithFormat:@"headquarters"];
        [type retain];
        defensiveBonus = 2;
    }
    return self;
}

-(void) setupAnim
{
    NSString *color;
    if (ourBuilding)
    {
        color = [NSString stringWithFormat:@"Blue"];
    }
    else
    {
        color = [NSString stringWithFormat:@"Red"];
    }
    [self removeChild:healthLabel cleanup:YES];
    NSString * health = [NSString stringWithFormat:@"%i", hp];
    healthLabel = [CCLabelTTF labelWithString:health fontName:@"Marker Felt" fontSize:10];
    [self addChild:healthLabel z:100];
    [healthLabel setColor:ccc3(0, 0, 0)];
    [healthLabel setPosition:ccp(6,7)];
    NSString* buildingLocationPath = [NSString stringWithFormat:@"building%@.png", color];
    CCSprite* buildingSprite = [CCSprite spriteWithFile:buildingLocationPath];
    buildingSprite.scale = .8;
    buildingSprite.position = ccp(13,15);
    [self addChild:buildingSprite];
    //CCSpriteFrame* buildingSprite = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:buildingLocationPath];
//[yourSprite setTexture:[[CCSprite spriteWithFile:@"yourImage.png"]texture]];
//    [self setTexture:[[CCSprite spriteWithFile:buildingLocationPath]texture] ];
    
    self.scale = 1.5;
   // self.position = ccp(10,10);
}
-(void) attack:(int)cap
{
    hp = hp - cap;
    if (hp <= 0)
    {
        NSLog(@"Game Over");
        if (ourBuilding)
        {
            NSLog(@"DEFEAT");
        }
        else
        {
            NSLog(@"VICTORY");
        }
    }
    capprogress = true;
    [self setupAnim];
}
-(void) reset
{
    hp = 10;
    capprogress = false;
    [self setupAnim];
}
@end
/*
 
 [
 
 //Switch between the various sprites
 if ([soldier_action isEqualToString:@"walking"])
 {
 soldier_sprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"soldier-bazooka-running-%@-frame-01.png", color]];
 soldier_sprite.scale = 2.0;
 //soldier_sprite.position = ccp(xlocation, ylocation);
 soldier_sprite.position = ccp(0, 0);
 [soldier_sprite runAction:self->walkAction];
 
 }
 else if ([soldier_action isEqualToString:@"shooting"])// if (isRunning)
 {
 NSLog(@"HELLO WORLD: 5");
 
 soldier_sprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"soldier-bazooka-shooting-%@-frame-01.png", color]];
 soldier_sprite.scale = 2.0;
 //soldier_sprite.position = ccp(xlocation, ylocation);
 soldier_sprite.position = ccp(0, 0);
 [soldier_sprite runAction:attackAction];
 }
 else {return;}
 
 [spriteSheet addChild:self->soldier_sprite];
 
 [self addChild:spriteSheet z:0];
 //[self draw];
*/