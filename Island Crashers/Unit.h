//
//  Unit.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/28/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"

@interface Unit : CCSprite
{
    
    //how much damage a unit does
    int firepower;
    //how much health a unit has. Affects the sprite being used (at the very least different number at the bottom)
    int hp;
    //how much defense a unit has
    int defense;
    //how far a unit can move
    int movement;
    int AttackDistance;
    //type of unit
    NSString* unit_type;
    NSString* color;
    int SCALE;
    CCLabelTTF *healthLabel;
    CCLabelTTF *identifierLabel; //shows if the soldier can attack (A), move (M) or none (X)
    BOOL canMove; //Holds whether or not this unit has moved.
    BOOL OurUnit;
    BOOL canAtt;
    int ID;
    CCSprite *soldier_sprite;
    int xPos, yPos;
    NSString* soldier_action;
    //HPLabelBackground *hplb;
    
    CCAction *walkAction;
    CCAction *attackAction;
    CCSpriteBatchNode *spriteSheet;
    
}
@property CCSprite* sprite;
@property BOOL isOurUnit;
@property int xloc;
@property int yloc;
@property int health;
@property NSString* uType;
@property int att;
@property int def;
@property CCAction* walkAct;
-(int) getAttackDistance;
-(BOOL) isMovable;
-(void) hasMoved;
-(void) setID:(int) id;
-(void) newTurn;
-(void) setCanAttackFalse;
-(void) setCanAttackTrue;
-(BOOL) getCanAttack;

- (void) triggerRepeatActionForSprite:(CCSprite*) sprite;
-(void) setlocationX: (int) x
                   Y: (int) y;
-(int) baseFirepower;
-(int) baseDefense;
-(NSString *) type;
-(void) setWalking;
-(void) setShooting;
-(int) getXPosition; //This is the tile location
-(int) getYPosition; //This is the tile location
-(int) getMovement;
-(CCSprite *) getSprite;
@end
