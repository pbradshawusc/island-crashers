//
//  BazookaSoldier.h
//  Island Crashers
//
//  Created by Joseph Lin on 3/31/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "Soldier.h"
#import "cocos2d.h"
@interface BazookaSoldier : Soldier
{
    SEL attackANIM;
}
@property SEL attackAnimationSelector;
-(NSString *) getUnitType;
@end
