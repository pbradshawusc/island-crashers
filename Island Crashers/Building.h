//
//  Building.h
//  Island Crashers
//
//  Created by ITP Student on 4/7/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"
//#import "HPLabelBackground.h"
@interface Building : CCSprite
{
    CCLabelTTF *healthLabel;
    int hp, xPosition, yPosition;
    int defensiveBonus;
    bool capprogress;
    //HPLabelBackground *hplb;
    BOOL ourBuilding;
    NSString *type;
}
@property int defensebonus;
@property int hqintegrity;
@property NSString* structureType;
@property int xloc;
@property int yloc;
@property bool captureProgress;
-(bool) isOurBuilding;
-(void) setOurBuilding: (bool) isMyBuilding;
-(void) setPositionX:(int) x Y:(int) y;
-(void) setHealth:(int) h;
-(bool) capture:(int)amt;
@end
