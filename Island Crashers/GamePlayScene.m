//
//  GamePlayScene.m
//  Island Crashers
//
//  Created by Edward Foux on 3/24/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "GamePlayScene.h"
#import "AppDelegate.h"
#import "MapLayer.h"
#import "UILayer.h"
#import "RifleSoldier.h"
#import "Building.h"
#import "HeadQuarters.h"
#import "Barracks.h"
#import "ResultsScene.h"


@class GameData;

@interface GamePlayScene()

@end

@implementation GamePlayScene
@synthesize gameNumber = gameNum;
@synthesize playerNumber = playerNum;
// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
    
	// 'layer' is an autorelease object.
	GamePlayScene *layer = [GamePlayScene node];
    
    
	// add layer as a child to scene
	[scene addChild: layer z:0];
    
    
	// return the scene
	return scene;
}

-(id) initWithGameNum:(int)gn PlayerNum:(int)pn OpName:(NSString*)op
{
	if( (self=[super init]) ) {
        //Start the Game Music
        [[GameData GetGameData] playGameMusic];
        //Set the Game Number and initial gameplay data
        gameNum = gn;
        playerNum = pn;
        
        ui = [UILayer node];
        gpd = [[GamePlayData alloc] initWithUILayer: ui];
        gpd.playerNumber = playerNum;
        gpd.gameNumber = gameNum;
        gpd.theWinner = -1;
        [ui setGamePlayData:gpd];
        [ui showOpponent:op];
        
        //Instantiate the Data representation
        MapLayer *map = [[MapLayer alloc] initWithGamePlayData:gpd];
        [ui setMapLayer:map];
        [self addChild:map z:0];
        [self addChild:ui z:1];
        
        //Load in the game data from Firebase
        loadedUsers = false;
        loadedTurn = false;
        loadedUnits = false;
        loadedStructures = false;
        exited = false;
        NSString *filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/players/%i/", gameNum, playerNum];
        Firebase* f = [[Firebase alloc] initWithUrl:filepath];
        [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
        {
            if(loadedUsers)
            {
                return;
            }
            bool money = false;
            for(FDataSnapshot* child in [snapshot children])
            {
                if([child.name isEqualToString:@"food"])
                {
                    gpd.currentFood = [child.value integerValue];
                    NSLog(@"Food: %i", gpd.currentFood);
                }
                else if([child.name isEqualToString:@"materials"])
                {
                    gpd.currentMaterials = [child.value integerValue];
                    NSLog(@"Materials: %i", gpd.currentMaterials);
                }
                else if([child.name isEqualToString:@"money"])
                {
                    gpd.currentMoney = [child.value integerValue];
                    NSLog(@"Money: %i", gpd.currentMoney);
                    money = true;
                }
                else if([child.name isEqualToString:@"defenseBonus"])
                {
                    gpd.defenseBonus = [child.value integerValue];
                    NSLog(@"Defense Bonus: %i", gpd.currentMoney);
                }
            }
            if(!money)
            {
                gpd.currentMoney = 1000;
            }
            loadedUsers = true;
        }];
        filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/", gameNum];
        fTurn = [[Firebase alloc] initWithUrl:filepath];
        [fTurn retain];
        fTurnHandle = [fTurn observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
         {
             if(exited)
             {
                 return;
             }
             for(FDataSnapshot* child in [snapshot children])
             {
                 if([child.name isEqualToString:@"currentTurn"])
                 {
                     if(loadedTurn)
                     {
                         if(gpd.currentTurn != [child.value integerValue])
                         {
                             gpd.currentTurn = [child.value integerValue];
                             NSLog(@"Turn: %i", gpd.currentTurn);
                             [self refreshScene];
                         }
                         return;
                     }
                     gpd.currentTurn = [child.value integerValue];
                     NSLog(@"Turn: %i", gpd.currentTurn);
                 }
             }
             loadedTurn = true;
         }];
        loadedWinner = false;
        filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/", gameNum];
        f = [[Firebase alloc] initWithUrl:filepath];
        [fTurn observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
        {
            if(exited)
            {
                return;
            }
            gpd.theWinner = -1;
            for(FDataSnapshot* child in [snapshot children])
            {
                if([child.name isEqualToString:@"winner"])
                {
                    gpd.theWinner = [child.value integerValue];
                    loadedWinner = true;
                }
            }
        }];
        
        filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/units/", gameNum];
        f = [[Firebase alloc] initWithUrl:filepath];
        [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
         {
             if(loadedUnits)
             {
                 return;
             }
             for(FDataSnapshot* child in [snapshot children])
             {
                 Unit *u;
                 for(FDataSnapshot* fds in [child children])
                 {
                     if([fds.name isEqualToString:@"type"])
                     {
                         if([fds.value isEqualToString:@"bazooka"])
                         {
                             u = [[[BazookaSoldier alloc] init] retain];
                         }
                         else if([fds.value isEqualToString:@"rifle"])
                         {
                             u = [[[RifleSoldier alloc] init] retain];
                         }
                     }
                 }
                 if(u == nil)
                 {
                     NSLog(@"Failed to read");
                     break;
                 }
                 int hp, player, xPos, yPos;
                 for(FDataSnapshot* fds in [child children])
                 {
                     if([fds.name isEqualToString:@"health"])
                     {
                         hp = [fds.value integerValue];
                     }
                     else if([fds.name isEqualToString:@"player"])
                     {
                         player = [fds.value integerValue];
                     }
                     else if([fds.name isEqualToString:@"locationX"])
                     {
                         xPos = [fds.value integerValue];
                     }
                     else if([fds.name isEqualToString:@"locationY"])
                     {
                         yPos = [fds.value integerValue];
                     }
                 }
                 u.xloc = xPos;
                 u.yloc = yPos;
                 u.health = hp;
                 if(player == playerNum)
                 {
                     u.isOurUnit = true;
                     [gpd addOurUnit:u];
                     NSLog(@"Added Ally %@ x: %i y: %i hp: %i", u.uType, xPos, yPos, hp);
                 }
                 else
                 {
                     u.isOurUnit = false;
                     [gpd addEnemyUnit:u];
                     NSLog(@"Added Enemy %@ x: %i y: %i hp: %i", u.uType, xPos, yPos, hp);
                 }
                 [map addUnit:u];
             }
             loadedUnits = true;
         }];
        
        filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%i/structures/", gameNum];
        f = [[Firebase alloc] initWithUrl:filepath];
        [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
         {
             if(loadedStructures)
             {
                 return;
             }
             bool failedToRead = true;
             for(FDataSnapshot* child in [snapshot children])
             {
                 failedToRead = false;
                 Building *b;
                 for(FDataSnapshot* fds in [child children])
                 {
                     if([fds.name isEqualToString:@"type"])
                     {
                         if([fds.value isEqualToString:@"headquarters"])
                         {
                             b = [[[HeadQuarters alloc] init] retain];
                         }
                         else if([fds.value isEqualToString:@"barracks"])
                         {
                             b = [[[Barracks alloc] init] retain];
                         }
                     }
                 }
                 if(b == nil)
                 {
                     failedToRead = true;
                     break;
                 }
                 int hp, xPos, yPos;
                 bool ours, capture;
                 for(FDataSnapshot* fds in [child children])
                 {
                     if([fds.name isEqualToString:@"health"])
                     {
                         hp = [fds.value integerValue];
                     }
                     else if([fds.name isEqualToString:@"captureProgress"])
                     {
                         capture = [fds.value boolValue];
                     }
                     else if([fds.name isEqualToString:@"owner"])
                     {
                         ours = ([fds.value integerValue] == [gpd playerNumber]);
                     }
                     else if([fds.name isEqualToString:@"locationX"])
                     {
                         xPos = [fds.value integerValue];
                     }
                     else if([fds.name isEqualToString:@"locationY"])
                     {
                         yPos = [fds.value integerValue];
                     }
                 }
                 [b setPositionX:xPos Y:yPos];
                 [b setHealth:hp];
                 [b setOurBuilding:ours];
                 b.captureProgress = capture;
                 if(ours)
                 {
                     [gpd addOurStructure:b];
                 }
                 else
                 {
                     [gpd addEnemyStructure:b];
                 }
                 [map addStructure:b];
             }
             if(failedToRead) //If this is reached then there was an error (most likely this means that there were no structures in firebase
             {
                 //[map loadNewStructures]; //This is not yet implemented, but it will read through the map and randomly create structures
                 [map initializeStructures];
             }
             loadedStructures = true;
         }];

        
        [ui setResourceManagerWithTroops: (NSMutableArray *) [gpd getTroopsWithPlayerID: playerNum] /*andStructures: (NSMutableArray *) structures andResources: (NSMutableArray *) resources*/];
        
        timeStarted = [NSDate timeIntervalSinceReferenceDate];
        
        [self scheduleUpdate];
    }
    return self;
}

-(void) update:(ccTime)delta
{
    NSTimeInterval diff = [NSDate timeIntervalSinceReferenceDate] - timeStarted;
    if(diff < 3)
    {
        return;
    }
    if(gpd.theWinner != -1)
    {
        [self unscheduleUpdate];
        if(gpd.theWinner == gpd.playerNumber)
        {
            [[GameData GetGameData] playMenuMusic];
            //[[CCDirector sharedDirector] stopAnimation];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[[ResultsScene alloc] initWithCondition:true AndGameNum:gameNum]]];
        }
        else
        {
            [[GameData GetGameData] playMenuMusic];
            //[[CCDirector sharedDirector] endAppearanceTransition];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[[ResultsScene alloc] initWithCondition:false AndGameNum:gameNum]]];
        }
    }
    else if(loadedWinner & loadedTurn && loadedUnits && loadedUsers && loadedStructures)
    {
        [ui updateMoneyLabel];
        [self unscheduleUpdate];
        if(gpd.currentTurn == gpd.playerNumber)
        {
            [ui showEndTurn];
        }
    }
}

-(GamePlayData *) getGamePlayData
{
    return gpd;
}
-(void) onExitTransitionDidStart
{
    //[fTurn removeObserverWithHandle:fTurnHandle];
    if(!exited)
    {
        [fTurn removeAllObservers];
        exited = true;
    }
    [super onExitTransitionDidStart];
}

-(void) refreshScene
{
    if(gpd.theWinner != -1)
    {
        return;
    }
    exited = true;
    [ui refreshScene];
}
@end