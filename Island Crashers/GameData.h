//
//  GameData.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/15/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>
#import <FirebaseSimpleLogin/FirebaseSimpleLogin.h>

@interface GameData : NSObject
{
    NSString *currentEmail;
    NSString *currentPassword;
    NSString *uniqueID;
    NSString *currentGames;
    NSNumber *experience;
    
    Firebase *f;
    FirebaseSimpleLogin* authClient;
    //FDataSnapshot*
    
    BOOL isLoggedIn;
    float tWidth;
}
@property NSString *email;
@property NSString *password;
@property NSString *uid;
@property NSString *games;
@property NSNumber* exp;
@property float tileWidth;
+(GameData *) GetGameData;
-(BOOL) logIn;
-(void) logOut;
-(BOOL) checkLoginStatus;
-(BOOL) createAccountWithEmail:(NSString *)em Password:(NSString *)pas;
-(void) saveUserData;
-(Firebase *) getFirebaseRoot;
-(void) playMenuMusic;
-(void) playGameMusic;
@end
