//
//  SpriteLayer.m
//  Island Crashers
//
//  Created by ITP Student on 4/7/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "SpriteLayer.h"

@implementation SpriteLayer
- (id)init
{
    self = [super init];
    if (self) {
        AllUnits = [[GamePlayData alloc] init];
        BuildingList = [[NSMutableArray alloc] init];
        
        
    }
    return self;
}
-(void)addUnit: (Unit *) a
{
    [AllUnits addOurUnit:a];
}
-(void)addBuilding: (Building *) a
{
    [BuildingList addObject:a];
}
@end
