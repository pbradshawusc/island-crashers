//
//  Building.m
//  Island Crashers
//
//  Created by ITP Student on 4/7/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "Building.h"

@implementation Building
@synthesize structureType = type;
@synthesize hqintegrity = hp;
@synthesize xloc = xPosition;
@synthesize yloc = yPosition;
@synthesize captureProgress = capprogress;
@synthesize defensebonus = defenseBonus;
- (id)init
{
    self = [super init];
    if (self) {
        hp = 10;
        capprogress = false;
        NSString *health = [NSString stringWithFormat:@"%i", hp];
        healthLabel = [CCLabelTTF labelWithString:health fontName:@"Marker Felt" fontSize:10];
        //[self addChild:hplb z:100];
    }
    return self;
}
-(void) setOurBuilding: (bool) isMyBuilding
{
    ourBuilding = isMyBuilding;
}
-(bool)isOurBuilding
{
    return ourBuilding;
}
-(void) setPositionX:(int) x Y:(int) y
{
    xPosition = x;
    yPosition = y;
}
-(void) setHealth:(int) h
{
    hp = h;
}
-(bool) capture:(int)amt
{
    hp-=amt;
    if(hp <= 0)
    {
        hp = 10;
        //Here we need to add the building to the opposite structure list than what it is currently in
        ourBuilding = !ourBuilding;
        return true;
    }
    return false;
}
@end
