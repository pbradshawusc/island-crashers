//
//  GameListScene.m
//  Island Crashers
//
//  Created by Alexa Rucks on 3/31/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "GameListScene.h"
#import "GamePlayScene.h"
#import "GameData.h"

@class MainMenuScene;

@interface user : NSObject
{
    NSString *uniqueID;
    int experience;
    NSString *email;
    NSString *games;
}
@property NSString* uid;
@property int exp;
@property NSString* mail;
@property NSString* myGames;
@end
@implementation user
@synthesize uid = uniqueID;
@synthesize exp = experience;
@synthesize mail = email;
@synthesize myGames = games;
@end

@implementation GameListScene
-(id) init
{
    if(self = [super init])
    {
        hitNewGame = false;
        newGameCreated = false;
        updatedOne = false;
        updatedTwo = false;
        num = -1;
        opponentName = nil;
        opponentUID = nil;
        pn = 1;
        CGSize size = [CCDirector sharedDirector].winSize;
        CCSprite *spr_nav = [CCSprite spriteWithFile:@"SPEECH_BOX.png" rect:CGRectMake(5,1,40,25)];
        spr_nav.scaleX = size.width/spr_nav.contentSize.width;
        spr_nav.scaleY = size.height/(spr_nav.contentSize.height*8);
        spr_nav.position = ccp(spr_nav.scaleX*spr_nav.contentSize.width/2, size.height-(spr_nav.scaleY*spr_nav.contentSize.height/2));
        [self addChild:spr_nav];
        
        CCMenuItem *itemBack = [CCMenuItemFont itemWithString:@"<Back" block:^(id sender) {
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuScene node] ]];
        }];
        CCMenu *menu = [CCMenu menuWithItems:itemBack, nil];
        [menu setColor:ccc3(0, 0, 0)];
        [menu setPosition:ccp( 3+itemBack.contentSize.width/2, size.height*15/16)];
        [self addChild:menu];
        
        CCMenuItem *itemNewGame = [CCMenuItemFont itemWithString:@"New Game" block:^(id sender) {
            if(hitNewGame || newGameCreated)
            {
                return;
            }
            num = -1;
            Firebase* f = [[Firebase alloc] initWithUrl:@"https://dazzling-fire-1374.firebaseio.com/users/"];
            
            [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
            {
                NSMutableArray* allUsers = [NSMutableArray arrayWithObjects:nil];
                
                for(FDataSnapshot* nse in [snapshot children])
                {
                    user* u = [[user alloc] init];
                    u.uid = nse.name;
                    u.exp = [nse childSnapshotForPath:@"experience"].value;
                    u.mail = [nse childSnapshotForPath:@"email"].value;
                    u.myGames = [nse childSnapshotForPath:@"currentGames"].value;
                    [u.myGames retain];
                    [allUsers addObject:u];
                }
                
                NSMutableArray* eligibleUsers = [NSMutableArray arrayWithObjects:nil];
                user* me;
                
                for(user* u in allUsers)
                {
                    if([u.uid isEqualToString:[GameData GetGameData].uid])
                    {
                        me = u;
                        //[allUsers removeObject:u];
                    }
                    else{
                        NSArray *tempArray = [u.myGames componentsSeparatedByString:@","];
                        if([tempArray count] < 6)
                        {
                            [eligibleUsers addObject:u];
                        }
                    }
                }
                
                user* u = nil;
                /*for(user* us in eligibleUsers){
                    if(u == nil)
                    {
                        u = us;
                    }
                    else if((u.exp - me.exp)*(u.exp - me.exp) > (us.exp-me.exp)*(us.exp-me.exp))
                    {
                        u = us;
                    }
                }*/
                while(u == nil)
                {
                    int randomChoice = arc4random()%[eligibleUsers count];
                    u = [eligibleUsers objectAtIndex:randomChoice];
                    if([eligibleUsers count] != 1 && (u.exp - me.exp)*(u.exp - me.exp) > 1000)
                    {
                        [eligibleUsers removeObject:u];
                        u = nil;
                    }
                }
                
                
                opponentName = u.mail;
                opponentUID = u.uid;
                pn = 1;
                NSLog(u.mail);
            }];
            
            //Get the Game Number
            NSString *filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/currentGameNum/"];
            f = [[Firebase alloc] initWithUrl:filepath];
            [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
             {
                 if(num == -1)
                 {
                     num = [snapshot.value integerValue];
                 }
             }];
            
            hitNewGame = true;
            newGameCreated = true;
            
           
            [self scheduleUpdate];
        }];
        CCMenu *gameMenu = [CCMenu menuWithItems:nil];
        int numGames = 0;
        @try
        {
            //if([GameData GetGameData] == nil || [GameData GetGameData].games == nil || ![[GameData GetGameData].games isMemberOfClass:[NSString class]])
            if([GameData GetGameData] == nil || [GameData GetGameData].games == nil)
            {
                [[GameData GetGameData] logIn];
                [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuScene node] ]];
                return self;
            }
            NSString *games = [NSString stringWithString:[GameData GetGameData].games];
            if(![games isEqualToString:@"none"] && ![games isEqualToString:@""])
            {
                NSArray *allGames = [games componentsSeparatedByString:@","];
                numGames += [allGames count];
                for(NSString *str in allGames)
                {
                    CCMenuItem *itemGame = [CCMenuItemFont itemWithString:str block:^(id sender) {
                        updatedOne = true;
                        updatedTwo = true;
                        num = [str intValue];
                        NSString *filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/%@/", str];
                        Firebase* f = [[Firebase alloc] initWithUrl:filepath];
                        [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
                        {
                           for(FDataSnapshot* child in [snapshot children])
                           {
                                if([child.name isEqualToString:@"players"])
                                {
                                    for(FDataSnapshot* nse in [child children])
                                    {
                                        if([nse.name isEqualToString:@"1"] && opponentName == nil)
                                        {
                                            pn = 2;
                                        }
                                        else if(opponentName == nil){
                                            pn = 1;
                                        }
                                        if([nse.name isEqualToString:@"1"] || [nse.name isEqualToString:@"2"])
                                        {
                                            for(FDataSnapshot* snap in [nse children])
                                            {
                                                if([snap.name isEqualToString:@"email"])
                                                {
                                                    if(![snap.value isEqualToString:[GameData GetGameData].email])
                                                    {
                                                        opponentName = [NSString stringWithString:snap.value];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }];
                        [self unscheduleUpdate];
                        [self scheduleUpdate];
                    }];
                    [gameMenu addChild:itemGame];
                }
            }
            /*else
            {
                [[GameData GetGameData] logIn];
                [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuScene node] ]];
                return self;
            }*/
            if(numGames < 6)
            {
                [gameMenu addChild: itemNewGame];
            }
            
        }
        @catch (NSException *e)
        {
            NSLog(@"Exception: %@", e);
            [[GameData GetGameData] logIn];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuScene node] ]];
        }
        [gameMenu alignItemsVertically];
        
        [gameMenu setPosition:ccp( size.width/2, size.height/2 - size.height/16)];
        [self addChild:gameMenu];
        
        opponentName = nil;
    }
    return self;
}
-(void) update:(ccTime)delta
{
    if(hitNewGame && num != -1 && opponentName != nil && opponentUID != nil)
    {
        //Create the Game
        NSString *filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/games/"];
        Firebase *f = [[Firebase alloc] initWithUrl:filepath];
        Firebase *f2 = [f childByAppendingPath:@"currentGameNum"];
        [f2 setValue:[NSNumber numberWithInt:(num+1)]];
        NSString * path = [NSString stringWithFormat:@"%i",num];
        f2 = [f childByAppendingPath:path];
        [f2 setValue:@{@"currentTurn" : [NSNumber numberWithInt:1], @"winner" : [NSNumber numberWithInt:-1]}];
        Firebase *f3 = [f2 childByAppendingPath:@"players"];
        Firebase *f4 = [f3 childByAppendingPath:@"1"];
        [f4 setValue:@{@"defenseBonus" : [NSNumber numberWithInt:0], @"food" : [NSNumber numberWithInt:0], @"materials" : [NSNumber numberWithInt:0], @"money" : [NSNumber numberWithInt:1000], @"email" : [NSString stringWithFormat:[GameData GetGameData].email]}];
        f4 = [f3 childByAppendingPath:@"2"];
        [f4 setValue:@{@"defenseBonus" : [NSNumber numberWithInt:0], @"food" : [NSNumber numberWithInt:0], @"materials" : [NSNumber numberWithInt:0], @"money" : [NSNumber numberWithInt:1000], @"email" : [NSString stringWithFormat:opponentName]}];
        
        //Update the users' game lists
        filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/users/%@/", [GameData GetGameData].uid];
        f = [[Firebase alloc] initWithUrl:filepath];
        [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snap)
         {
             if(updatedTwo)
             {
                 return;
             }
             for(FDataSnapshot*snapshot in [snap children])
             {
                 NSLog(snapshot.name);
                 if([snapshot.name isEqualToString:@"currentGames"])
                 {
                     NSString* gameList = [NSString stringWithFormat:snapshot.value];
                     if([gameList isEqualToString:@""]||[gameList isEqualToString:@"none"])
                     {
                         gameList = [NSString stringWithFormat:@"%i",num];
                     }
                     else
                     {
                         gameList = [NSString stringWithFormat:@"%@,%i",gameList,num];
                     }
                     Firebase *f2 = [f childByAppendingPath:@"currentGames"];
                     [f2 setValue: gameList];
                     updatedTwo = true;
                 }
             }
         }];
        filepath = [NSString stringWithFormat:@"https://dazzling-fire-1374.firebaseio.com/users/%@/", opponentUID];
        f = [[Firebase alloc] initWithUrl:filepath];
        [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snap)
         {
             if(updatedOne)
             {
                 return;
             }
             for(FDataSnapshot*snapshot in [snap children])
             {
                 NSLog(snapshot.name);
                 if([snapshot.name isEqualToString:@"currentGames"])
                 {
                     NSString* gameList = [NSString stringWithFormat:snapshot.value];
                     gameList = [NSString stringWithFormat:@"%@,%i",gameList,num];
                    Firebase *f2 = [f childByAppendingPath:@"currentGames"];
                     [f2 setValue: gameList];
                     updatedOne = true;
                 }
             }
         }];
        
        hitNewGame = false;
    }
    else if(!hitNewGame && opponentName != nil && updatedOne && updatedTwo)
    {
        NSLog(opponentName);
        NSLog(@"%i, %i", num, pn);
        [self unscheduleUpdate];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[[GamePlayScene alloc] initWithGameNum:num PlayerNum:pn OpName:opponentName]]];
    }
}

@end
