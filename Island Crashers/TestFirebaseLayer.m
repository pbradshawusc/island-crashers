//
//  HelloWorldLayer.m
//  Island Crashers
//
//  Created by Alexa Rucks on 3/12/14.
//  Copyright iON Industries 2014. All rights reserved.
//


// Import the interfaces
#import "TestFirebaseLayer.h"
#import "MainMenuScene.h"
@class AccountInfoScene;

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation TestFirebaseLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	TestFirebaseLayer *layer = [TestFirebaseLayer node];
	
 
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}


// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
        data = [GameData GetGameData];
		
		// create and initialize a Label
		titleLabel = [CCLabelTTF labelWithString:@"Not Logged In" fontName:@"Marker Felt" fontSize:34];

		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
	
		// position the label on the center of the screen
		titleLabel.position =  ccp( size.width /2 , size.height-30 );
		
		// add the label as a child to this Layer
		[self addChild: titleLabel];
		
		// Default font size will be 28 points.
		[CCMenuItemFont setFontSize:28];
		
		// Achievement Menu Item using blocks
        CCMenuItemLabel *itemCheckStatus = [CCMenuItemFont itemWithString:@"Check Status" block:^(id sender){
            CCMenuItemLabel *lab = (CCMenuItemLabel *) sender;
            if([data checkLoginStatus])
            {
                //[data saveUserData];
                
                lab.color = ccGREEN;
                [titleLabel removeFromParentAndCleanup:YES];
                
                // create and initialize a Label
                NSString *userID = [NSString stringWithFormat:@"%@", data.email];
                titleLabel = [CCLabelTTF labelWithString:userID fontName:@"Marker Felt" fontSize:34];
                
                // ask director for the window size
                CGSize size = [[CCDirector sharedDirector] winSize];
                
                // position the label on the center of the screen
                titleLabel.position =  ccp( size.width /2 , size.height-30 );
                titleLabel.scaleX = size.width/titleLabel.contentSize.width;
                
                [self addChild:titleLabel];
            }
            else
            {
                lab.color = ccRED;
            }
        }];
		CCMenuItem *itemLogin = [CCMenuItemFont itemWithString:@"Log In" block:^(id sender) {
            if([data logIn])
            {
                [titleLabel removeFromParentAndCleanup:YES];
                
                // create and initialize a Label
                NSString *userID = [NSString stringWithFormat:@"%@", data.email];
                titleLabel = [CCLabelTTF labelWithString:userID fontName:@"Marker Felt" fontSize:34];
                
                // ask director for the window size
                CGSize size = [[CCDirector sharedDirector] winSize];
                
                // position the label on the center of the screen
                titleLabel.position =  ccp( size.width /2 , size.height-30 );
                titleLabel.scaleX = size.width/titleLabel.contentSize.width;
                
                [self addChild:titleLabel];
                itemCheckStatus.color = ccGREEN;
            }
		}];
		
		// Leaderboard Menu Item using blocks
		CCMenuItem *itemLogout = [CCMenuItemFont itemWithString:@"Log Out" block:^(id sender) {
			
            [data logOut];
            
            [titleLabel removeFromParentAndCleanup:YES];
            
            // create and initialize a Label
            titleLabel = [CCLabelTTF labelWithString:@"Not Logged In" fontName:@"Marker Felt" fontSize:34];
            
            // ask director for the window size
            CGSize size = [[CCDirector sharedDirector] winSize];
            
            // position the label on the center of the screen
            titleLabel.position =  ccp( size.width /2 , size.height-30 );
            
            [self addChild:titleLabel];
            itemCheckStatus.color = ccRED;
		}];
        
        
        
        CCMenuItemLabel *itemReturnToMainMenu = [CCMenuItemFont itemWithString:@"Back" block:^(id sender){
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[AccountInfoScene node] ]];
        }];

		
		CCMenu *menu = [CCMenu menuWithItems:itemLogin, itemLogout, itemCheckStatus, itemReturnToMainMenu, nil];
		
		[menu alignItemsVerticallyWithPadding:20];
		[menu setPosition:ccp( size.width/2, size.height/2 - 50)];
		
		// Add the menu to the layer
		[self addChild:menu];

	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
