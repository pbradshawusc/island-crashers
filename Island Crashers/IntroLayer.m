//
//  IntroLayer.m
//  Island Crashers
//
//  Created by Alexa Rucks on 3/12/14.
//  Copyright iON Industries 2014. All rights reserved.
//


// Import the interfaces
#import "IntroLayer.h"
#import "MainMenuScene.h"
#import "LoginScene.h"


#pragma mark - IntroLayer

// HelloWorldLayer implementation
@implementation IntroLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	IntroLayer *layer = [IntroLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// 
-(id) init
{
	if( (self=[super init])) {
        clicked = false;
        
        data = [GameData GetGameData];
        [data playMenuMusic];

		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];

		CCSprite *background;
		
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"Default-568h@2x~iphone.png"];
			background.rotation = 0;
		} else {
			background = [CCSprite spriteWithFile:@"Default-Landscape~ipad.png"];
		}
		background.position = ccp(size.width/2, size.height/2);

		// add the label as a child to this Layer
		[self addChild: background];
        
        CCMenuItemFont *itemEnter = [CCMenuItemFont itemWithString:@"Enter" block:^(id sender) {
            if(clicked)
            {
                return;
            }
            clicked = true;
            //[itemEnter setIsEnabled:NO];
            [self logIn];
        }];
        //[itemEnter setColor: ccc3(255,0,0)];
        CCMenu *menu = [CCMenu menuWithItems:itemEnter, nil];
        [menu setColor:ccc3(255, 0, 0)];
        [menu setPosition:ccp( size.width/2, size.height/6)];
        [self addChild:menu];
	}
	
	return self;
}

-(void) onEnter
{
	[super onEnter];
}

-(void) logIn
{
    timePassed = 0.0;
    [data logIn];
    [self scheduleUpdate];
}

-(void) update:(ccTime)delta
{
    timePassed += delta;
    if([data checkLoginStatus])
    {
        [self unscheduleUpdate];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuScene node] ]];
    }
    else if(timePassed > 5.0)
    {
        [self  unscheduleUpdate];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[LoginScene scene] ]];
    }
}
@end
