//
//  LoginScene.h
//  Island Crashers
//
//  Created by Alexa Rucks on 3/16/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "cocos2d.h"
#import "CCUIViewWrapper.h"
#import "GameData.h"

@interface LoginScene : CCLayer
{
    UITextField *emailTextField;
    CCUIViewWrapper *emailWrapper;
    UITextField *passwordTextField;
    CCUIViewWrapper *passwordWrapper;
    UIButton *submitButton;
    CCUIViewWrapper *submitWrapper;
    
    CCLabelTTF *connectingLabel;
    
    GameData *data;
    double timePassed;
}
+(CCScene *) scene;
-(void) update:(ccTime)delta;
@end
