//
//  MovableSprite.h
//  Island Crashers
//
//  Created by James Lynch on 4/14/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCSprite.h"
#import "cocos2d.h"
#import "Unit.h"

@interface MovableSprite : Unit
{
    //CCSpriteBatchNode *spriteSheet;
    //CCAction *walkAction;
    //NSString* soldier_action;
    //CCSprite *soldier_sprite;
}

- (void) setupAnim;
@end
