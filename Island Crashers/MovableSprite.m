//
//  MovableSprite.m
//  Island Crashers
//
//  Created by James Lynch on 4/14/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "MovableSprite.h"

@implementation MovableSprite
-(id) init
{
    if (self = [super init])
    {
    }
    return self;
}
- (void) setupAnim
{
    CGSize size = [[CCDirector sharedDirector] winSize];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"soldier-red.plist"];
    [spriteSheet removeAllChildrenWithCleanup:YES];
    spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"soldier-red.png"];
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    for (int i=1; i<=6; i++) {
        [walkAnimFrames addObject:
        [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"soldier-bazooka-running-red-frame-0%d.png",i]]];
    }

    CCAnimation *walkAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:0.1f];
    walkAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:walkAnim]];
        
    //Switch between the various sprites
    if ([soldier_action isEqualToString:@"walking"])
    {
        soldier_sprite = [CCSprite spriteWithSpriteFrameName:@"soldier bazooka-running-red-frame-01.png"];
        soldier_sprite.scale = 2.0;
        soldier_sprite.position = ccp(0, 0);
        [soldier_sprite runAction:self->walkAction];
    }
    else {return;}
        
    [spriteSheet addChild:self->soldier_sprite];
        
    [self addChild:spriteSheet z:0];
        
}

@end
