//
//  ResultsScene.m
//  Island Crashers
//
//  Created by Alexa Rucks on 4/27/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "ResultsScene.h"
#import "MainMenuScene.h"
#import "GameData.h"

@implementation ResultsScene
-(id) init
{
    self = [super init];
    // ask director for the window size
    CGSize size = [[CCDirector sharedDirector] winSize];
    //Set the background for the layer
    CCSprite *background = [CCSprite spriteWithFile:@"MenuBackground.png"];
    background.position = ccp(size.width/2, size.height/2);
    background.scaleY = 0.95;
    [self addChild: background];
    return self;
}
-(id) initWithCondition:(bool)didWin AndGameNum:(int)g
{
    self = [self init];
    gameNum = g;
    CGSize size = [[CCDirector sharedDirector] winSize];
    back = false;
    GameData *gameData = [GameData GetGameData];
    if(didWin)
    {
        CCLabelTTF *titleLabel = [CCLabelTTF labelWithString:@"You Win!" fontName:@"Marker Felt" fontSize:34];
        [titleLabel setPosition:ccp(size.width/2, size.height*7/9)];
        [titleLabel setColor: ccc3(0,0,0)];
        [self addChild:titleLabel];
        
        CCLabelTTF *expLabel = [CCLabelTTF labelWithString:@"+ Experience: 300" fontName:@"Marker Felt" fontSize:34];
        [expLabel setPosition:ccp(size.width/2, size.height/2)];
        [expLabel setColor: ccc3(0,0,0)];
        [self addChild:expLabel];
        gameData.exp = [NSNumber numberWithInt:([gameData.exp intValue]+300)];
        [gameData.exp retain];
    }
    else
    {
        CCLabelTTF *titleLabel = [CCLabelTTF labelWithString:@"You Lose!" fontName:@"Marker Felt" fontSize:34];
        [titleLabel setPosition:ccp(size.width/2, size.height*7/9)];
        [titleLabel setColor: ccc3(0,0,0)];
        [self addChild:titleLabel];
        
        CCLabelTTF *expLabel = [CCLabelTTF labelWithString:@"+ Experience: 100" fontName:@"Marker Felt" fontSize:34];
        [expLabel setPosition:ccp(size.width/2, size.height/2)];
        [expLabel setColor: ccc3(0,0,0)];
        [self addChild:expLabel];
        gameData.exp = [NSNumber numberWithInt:([gameData.exp intValue]+100)];
        [gameData.exp retain];
    }
    //We also need to be able to return to the main menu
    CCMenuItem *itemBack = [CCMenuItemFont itemWithString:@"Return to Menu" block:^(id sender) {
        if(back)
        {
            return;
        }
        back = true;
        [[CCDirector sharedDirector] replaceScene:[CCTransitionPageTurn transitionWithDuration:1.0 scene:[MainMenuScene node] backwards:YES]];
    }];
    
    CCMenu *backMenu = [CCMenu menuWithItems:itemBack, nil];
    [backMenu setColor:ccc3(0, 0, 0)];
    [backMenu setPosition:ccp( size.width/2, size.height/2-30)];
    [self addChild:backMenu];
    
    //Remove the game from the user list, and have it write to firebase
    NSString *games = gameData.games;
    NSArray *gameList = [games componentsSeparatedByString:@","];
    games = [NSString stringWithFormat:@""];
    for(NSString *str in gameList)
    {
        if(!([str intValue] == gameNum))
        {
            games = [NSString stringWithFormat:@"%@%@,", games, str];
            
        }
    }
    games = [games substringToIndex:[games length]-1];
    [games retain];
    gameData.games = games;
    [gameData saveUserData];
    return self;
}
@end
