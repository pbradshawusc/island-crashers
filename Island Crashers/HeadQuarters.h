//
//  HeadQuarters.h
//  Island Crashers
//
//  Created by ITP Student on 3/31/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Building.h"
@interface HeadQuarters : Building
-(void) attack: (int) cap;
-(void) reset;
@end
