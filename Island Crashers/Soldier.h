//
//  Soldier.h
//  Island Crashers
//
//  Created by Joseph Lin on 3/24/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"
#import "Unit.h"
@interface Soldier : Unit
{
    
    //how much damage a Soldier does
    //int firepower;
    //how much health a Soldier has. Affects the sprite being used (at the very least different number at the bottom)
    //int hp;
    //how much defense a Solider has
    //int defense;
    //how far a Soldier can move
    //int movement;
    //type of soldier
    
    //NSString* color;

    
    
}

//@property NSString* _type;

-(int) baseFirepower;
-(int) baseDefense;
-(void) setShooting;
-(void) update:(ccTime) dt;
-(NSString *) type;

@end
