//
//  AccountInfoScene.h
//  Island Crashers
//
//  Created by Alexa Rucks on 4/25/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "CCScene.h"
#import "cocos2d.h"
#import "GameData.h"

@interface AccountInfoScene : CCScene
{
    GameData *data;
    bool back;
}
@end
