//
//  Barracks.m
//  Island Crashers
//
//  Created by ITP Student on 4/7/14.
//  Copyright (c) 2014 iON Industries. All rights reserved.
//

#import "Barracks.h"

@implementation Barracks
-(id) init
{
    self = [super init];
    type = [NSString stringWithFormat:@"barracks"];
    [type retain];
    defensiveBonus = 2;
    return self;
}
-(void) setupAnim
{
    NSString *color;
    if (ourBuilding)
    {
        color = [NSString stringWithFormat:@"Blue"];
    }
    else
    {
        color = [NSString stringWithFormat:@"Red"];
    }
    [self removeChild:healthLabel cleanup:YES];
//    NSString * health = [NSString stringWithFormat:@"%i", hp];
//    healthLabel = [CCLabelTTF labelWithString:health fontName:@"Marker Felt" fontSize:10];
//    [self addChild:healthLabel z:100];
//    [healthLabel setColor:ccc3(255, 0, 0)];
//    [healthLabel setPosition:ccp(healthLabel.contentSize.width/2,healthLabel.contentSize.height/2)];
    CCSprite *buildingSprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"barak%@.png", color]];
    [self addChild:buildingSprite];
    buildingSprite.scaleY = 1.75;
    buildingSprite.scaleX = 2;
    buildingSprite.position = ccp(buildingSprite.contentSize.width*buildingSprite.scaleX/2, buildingSprite.contentSize.height*buildingSprite.scaleY/2);
    //[self initWithSpriteFrameName:[NSString stringWithFormat:@"barak%@.png", color]];
    //self.scale = 2.0;
    //self.position = ccp(0,0);
}
-(void) attack:(int)cap
{
    hp = hp - cap;
    if (hp <= 0)
    {
        ourBuilding = !ourBuilding;
    }
    capprogress = true;
    [self setupAnim];
}
-(void) reset
{
    hp = 10;
    capprogress = false;
    [self setupAnim];
}
@end
